---
audio: https://express.sfo2.digitaloceanspaces.com/aim/paj8tgKJf48.m4a
audio_duration: 00:38:11
date: 2018-10-15 19:27:55
filename: paj8tgKJf48.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Collapse of the Federal Reserve has started
year: '2018'
---

Did you know that the Federal Reserve has begun to collapse? Listen to Betsy and Thomas explain, then continue your education here: https://truthbits.blog/2018/10/15/collapse-of-the-federal-reserve-has-started/