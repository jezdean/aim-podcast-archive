---
audio: https://express.sfo2.digitaloceanspaces.com/aim/irT-yXuz6_4.m4a
audio_duration: 00:33:08
date: 2019-08-07 14:19:17
filename: irT-yXuz6_4.m4a
playlist: AIM Insights
publisher: AIM
title: Hidden History of the Grail Queens Revealed
year: '2019'
---

The AIM - AFI journey for the Holy Grail continues. Michael McKibben and Tyla Gabriel introduce Douglas Gabriel's newest book to our truth community. Fascinating conversation about a history never told. 

To read the first chapter, please go here: https://neoanthroposophy.com/2019/08/07/hidden-history-of-the-grail-queens/