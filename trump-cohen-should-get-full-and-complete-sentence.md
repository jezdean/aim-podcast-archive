---
audio: https://express.sfo2.digitaloceanspaces.com/aim/eROKCB9tsPM.m4a
audio_duration: 00:48:44
date: 2018-12-03 22:23:56
filename: eROKCB9tsPM.m4a
playlist: Betsy and Thomas
publisher: AIM
title: 'Trump: Cohen should get full and complete sentence'
year: '2018'
---

“I will never testify against Trump.” This statement was recently made by Roger Stone, essentially stating that he will not be forced by a rogue and out of control prosecutor to make up lies and stories about “President Trump.” Nice to know that some people still have “guts!” Read the tweets and learn about thorium here: https://truthbits.blog/2018/12/03/trump-michael-cohen-should-get-full-and-complete-sentence/