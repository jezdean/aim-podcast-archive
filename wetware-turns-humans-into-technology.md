---
audio: https://express.sfo2.digitaloceanspaces.com/aim/6Sl9ZF1wcX4.m4a
audio_duration: 00:25:43
date: 2018-02-24 14:33:11
filename: 6Sl9ZF1wcX4.m4a
playlist: AIM Insights
publisher: AIM
title: Wetware Turns Humans into Technology
year: '2018'
---

We don't want to become robot extensions for the corrupt Silicon Valley boy kings so we came up with a citizens' solution. This is not some pie-in-the-sky solution. This is something we can do to free humanity. Please educate and inform others.