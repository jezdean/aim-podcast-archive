---
audio: https://express.sfo2.digitaloceanspaces.com/aim/Mz8Ebt4UEK4.m4a
audio_duration: 00:23:16
date: 2018-11-08 18:06:35
filename: Mz8Ebt4UEK4.m4a
playlist: Swamp with Strunk
publisher: AIM
title: Secret Service calls Strunk for a meeting
year: '2018'
---

We disabled comments on the YouTube version of this video. If you have pertinent comments to share, please leave them in the comment box inside the article link. As we move deeper into "enemy territory", we aren't interested in random thoughts from trolls and low-IQ viewers. This information is for our white hats and independent media creators. 

Continue your citizen education here: https://patriots4truth.org/2018/11/08/strunk-meets-with-secret-service/