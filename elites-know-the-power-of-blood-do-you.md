---
audio: https://express.sfo2.digitaloceanspaces.com/aim/kbIDCJLjeDo.m4a
audio_duration: 00:35:39
date: 2019-09-17 21:23:11
filename: kbIDCJLjeDo.m4a
playlist: AIM Insights
publisher: AIM
title: Elites know the power of blood...do you?
year: '2019'
---

Douglas Gabriel and John Barnwell discuss the mysteries of blood and why the elite sickos of the world can't get enough. 

Listen to Leo and John discuss this topic and also learn about the Grail Queens and the blood relics here: https://truthbits.blog/2019/09/17/elites-know-the-power-of-blood/