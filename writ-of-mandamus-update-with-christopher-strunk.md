---
audio: https://express.sfo2.digitaloceanspaces.com/aim/7UDvwNMABps.m4a
audio_duration: 00:46:59
date: 2018-11-05 21:00:31
filename: 7UDvwNMABps.m4a
playlist: Betsy and Thomas
publisher: AIM
title: WRIT OF MANDAMUS Update with Christopher Strunk
year: '2018'
---

Douglas Gabriel speaks to our “man in the muck” in the Washington swamp – Christopher E. Strunk – who gives us the latest update of the Writ of Mandamus filed with SCOTUS plus some surprises that will have your head spinning as it did when Douglas heard Chris make these revelations. Here is the Writ: https://aim4truthblog.files.wordpress.com/2018/10/scotus-rule-22-3-writ-of-mandamus-uscaaf-for-preserve-paper-ballots-10-29-2018.pdf