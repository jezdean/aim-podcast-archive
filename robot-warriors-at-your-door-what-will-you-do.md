---
audio: https://express.sfo2.digitaloceanspaces.com/aim/p6-Sgx7rB2w.m4a
audio_duration: 00:46:36
date: 2019-08-13 18:05:11
filename: p6-Sgx7rB2w.m4a
playlist: Betsy and Thomas
publisher: AIM
title: ROBOT WARRIORS at your door - What will you do?
year: '2019'
---

Betsy and Thomas rail against the evil tech lords and their sinister plan to take over the world. Calling on all information warriors to step up the meme warfare. We have a planet to save and not a minute to spare.