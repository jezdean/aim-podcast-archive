---
audio: https://express.sfo2.digitaloceanspaces.com/aim/sKwbpfvpLC4.m4a
audio_duration: 00:26:30
date: 2017-11-21 22:47:22
filename: sKwbpfvpLC4.m4a
playlist: Leader Tech
publisher: AIM
title: Military Cyber Warfighting
year: '2017'
---

Full series of interviews here: https://aim4truth.org/2017/11/21/facebook-unmasked-how-the-worlds-most-relevant-entrepreneur-was-screwed-by-zuckerberg/

When Michael McKibben and his software engineers created their quantum leap in technology to almost infinite scalability with their social networking invention, they planned to implement a moral system that respected personal property and privacy and envisioned a new “age of freedom of information.” While Leader Technologies assumed that the government might want to use their invention, they had no way of knowing that the government intended to use it for the exact opposite moral purposes: to take away the user’s freedom and surveil everything in their lives.

Michael was trying to advance the industry and certainly would have never believed that his own government might become his enemy as he resisted the whole-sale theft and weaponization of his inventions. How could Leader Technologies know that the most-respected patent attorney in America was also the most corrupt?