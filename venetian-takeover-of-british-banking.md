---
audio: https://express.sfo2.digitaloceanspaces.com/aim/UK5TCUvPk7k.m4a
audio_duration: 00:27:39
date: 2019-04-01 17:56:19
filename: UK5TCUvPk7k.m4a
playlist: History of Banking
publisher: AIM
title: Venetian Takeover of British Banking
year: '2019'
---

Dr. Douglas Gabriel summarizes The Venetian Takeover of English Banking which brings to light the Venetian plan to supplant the Roman Catholic Pope, the European monarchy and any others who tried to slow down their insatiable desire for hegemony. The Venetian Republic became the new model for ruling a kingdom through multiple committees of oligarchs who exercise total control over the civic population, including imprisonment for debt or even assassination of those who got in the way of the bankers making money. The financing of war became the biggest business these warlord bankers conducted. The control of kings, queens, popes and despots fell to the control of central banks that came to rule most European nations as monarchs were transplanted by Italian Republics.

Read the full report here: https://truthbits.blog/2019/04/01/venetian-takeover-of-british-banking/

Access the full lesson on banking with all audios here: https://aim4truth.org/2019/04/03/god-or-mammon/