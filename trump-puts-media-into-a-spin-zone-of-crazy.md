---
audio: https://express.sfo2.digitaloceanspaces.com/aim/lO7rHnUBSns.m4a
audio_duration: 00:42:54
date: 2018-07-17 22:36:47
filename: lO7rHnUBSns.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Trump puts Media into a Spin Zone of Crazy
year: '2018'
---

Betsy and Thomas decode Trump’s tweets of July 17, 2018. Just as they start, a surprise member of the Conclave member walks in and the conversation becomes even more interesting.  To read the tweets as you listen and see other interesting information: https://truthbits.blog/2018/07/17/trump-puts-the-media-into-a-spin-zone-of-crazy/