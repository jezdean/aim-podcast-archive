---
audio: https://express.sfo2.digitaloceanspaces.com/aim/yHOv64RqwLY.m4a
audio_duration: 00:47:13
date: 2018-05-01 20:18:39
filename: yHOv64RqwLY.m4a
playlist: AIM Insights
publisher: AIM
title: Patent Theft Destroys American Innovation and Lives
year: '2018'
---

Douglas Gabriel (Thomas Paine) and Michael McKibben of Leader Technologies introduce Doug Fischerrer of the Fayette Regional Hospital in Indiana who explains the relationship between the opioid crisis in his community and the theft of social media.  

To learn about the theft of social media see this article: https://aim4truth.org/2017/11/21/facebook-unmasked-how-the-worlds-most-relevant-entrepreneur-was-screwed-by-zuckerberg/

To learn more about the drug crisis in America and who is behind it: https://aim4truth.org/2017/03/26/5663/