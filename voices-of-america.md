---
audio: https://express.sfo2.digitaloceanspaces.com/aim/Total-destruction-of-education-in-America.mp3
audio_duration: 00:53:58
date: 2020-08-18 00:00:00
filename: Total-destruction-of-education-in-America.mp3
publisher: AIM
title: Voices of America
year: '2020'
---

Listen to Betsy and Thomas describe our new format, going forward. Also a sneak peek on the breaking historical news the miners are finding on Henry Kissinger.