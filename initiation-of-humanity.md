---
audio: https://express.sfo2.digitaloceanspaces.com/aim/initiation-of-humanity.mp3
audio_duration: 00:48:14
date: 2020-04-11 00:00:00
filename: initiation-of-humanity.mp3
publisher: AIM
title: Initiation of Humanity
year: '2020'
---

There has been no other Easter on earth like the one we are all, globally, experiencing. Listen to Tyla and Douglas Gabriel explain why this is the great awakening and initiation of humanity.