---
audio: https://express.sfo2.digitaloceanspaces.com/aim/Q3YnL4Rmwm8.m4a
audio_duration: 00:43:26
date: 2018-07-22 18:28:36
filename: Q3YnL4Rmwm8.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Rats and Rodenstein Fill the Swamp
year: '2018'
---

Betsy and Thomas decode Trump’s tweets of July 22, 2018. The heavily redacted Carter Page FISA warrant is released, but Thomas reads between the lines and under the redactions as he decodes today’s tweets. To read the tweets as you listen and see other interesting information: https://truthbits.blog/2018/07/22/rats-and-rodentstein-fill-the-swamp/