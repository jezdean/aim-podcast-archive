---
audio: https://express.sfo2.digitaloceanspaces.com/aim/Deport-Kamala-Harris.mp3
audio_duration: 00:11:06
date: 2020-08-11 00:00:00
filename: Deport-Kamala-Harris.mp3
publisher: AIM
title: Kamala Harris has No Proof of Citizenship
year: '2020'
---

Tyla, Douglas, and Michael explain why Kamala Harris must immediately disclose her naturalization papers. We do not see any proof that she is a United States citizen. Read more and see the proof yourself:

https://patriots4truth.org/2020/08/11/kamala-harris-is-not-a-united-states-citizen/