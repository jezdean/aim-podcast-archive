---
audio: https://express.sfo2.digitaloceanspaces.com/aim/Happy-Hour-with-Fever-Tree-and-Zinc.mp3
audio_duration: 00:46:36
date: 2020-08-08 00:00:00
filename: Happy-Hour-with-Fever-Tree-and-Zinc.mp3
publisher: AIM
title: 'Betsy & Thomas Corona Cocktail Hour: Readers'' Questions Answered'
year: '2020'
---

Readers' questions are answered by Betsy and Thomas in this audio.

https://truthbits.blog/2020/08/08/corona-cocktail-hour/