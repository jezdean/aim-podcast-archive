---
audio: https://express.sfo2.digitaloceanspaces.com/aim/migPH1oBw-E.m4a
audio_duration: 00:25:14
date: 2018-04-20 18:40:55
filename: migPH1oBw-E.m4a
playlist: Betsy and Thomas
publisher: AIM
title: We Love WINNING!
year: '2018'
---

We love winning. Listen to Betsy and Thomas explain what other channels might not be telling you. 
The article on Rod Rosenstein’s wife is here: https://patriots4truth.org/2018/03/27/meet-lisa-h-barsoomian-the-wife-of-who/