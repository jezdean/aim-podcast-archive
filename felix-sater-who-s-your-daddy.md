---
audio: https://express.sfo2.digitaloceanspaces.com/aim/JAaNFcFpT5k.m4a
audio_duration: 00:24:02
date: 2018-03-13 01:31:41
filename: JAaNFcFpT5k.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Felix Sater- Who's Your Daddy?
year: '2018'
---

Betsy and Thomas discuss the backstory of Felix Sater and why his role as James Comey’s water boy might be coming back to haunt him. 

Articles referenced in the discussion are: 

http://nymag.com/daily/intelligencer/2017/08/felix-sater-donald-trump-russia-investigation.html 

https://www.zerohedge.com/news/2018-03-12/man-cited-trumps-russian-link-actually-works-fbi

The Felix Sater -Russia - James Comey Connection: https://www.youtube.com/watch?v=1L2JHsyzjIw&t=256s