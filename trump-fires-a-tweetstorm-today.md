---
audio: https://express.sfo2.digitaloceanspaces.com/aim/3E56NuudM6E.m4a
audio_duration: 00:57:35
date: 2018-06-07 19:09:31
filename: 3E56NuudM6E.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Trump fires a tweetstorm today
year: '2018'
---

Betsy and Thomas review Trump Tweets of June 7. Link here for audio and tweets in one URL: https://truthbits.blog/2018/06/07/decoding-trump-tweets-june-7-2018/

Bypass YouTube altogether and follow www.truthbits.blog to receive Trump Tweet decodes as they are posted. Audio and tweets all on one page. Plus, special surprises at the bottom of the page.