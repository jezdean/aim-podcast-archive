---
audio: https://express.sfo2.digitaloceanspaces.com/aim/GYGrd96lld4.m4a
audio_duration: 00:48:39
date: 2018-10-09 19:11:32
filename: GYGrd96lld4.m4a
playlist: AIM Insights
publisher: AIM
title: The Web of Corporate Oligarchy Ensnares Them
year: '2018'
---

Douglas Gabriel and John Barnwell discuss the historical aspects of how the world has become ensnared into a web of lies and deceit. Connect the dots between the City of London, Vatican City, the Queen’s Privy Council, and Senior Executive Service.