---
audio: https://express.sfo2.digitaloceanspaces.com/aim/VySZn65VCsw.m4a
audio_duration: 00:58:05
date: 2018-05-18 16:43:37
filename: VySZn65VCsw.m4a
playlist: AIM Insights
publisher: AIM
title: Eye of the Storm Passes Over- Second Storm Approaches
year: '2018'
---

Douglas Gabriel and John Barnwell take us deeper in our analysis of the corrupt systems in D. C. --including Senior Executive Service, Serco, British control of the U.S., the Fabian Society, Cloward and Piven, and the global coup d'etat.