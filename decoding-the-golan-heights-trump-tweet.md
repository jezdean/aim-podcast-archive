---
audio: https://express.sfo2.digitaloceanspaces.com/aim/I3Jdmr9aJOo.m4a
audio_duration: 00:53:11
date: 2019-03-23 17:37:37
filename: I3Jdmr9aJOo.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Decoding the 'Golan Heights" Trump Tweet
year: '2019'
---

Betsy and Thomas look at the results of the Mueller Report as well as a tweet from March 21, 2019 about the Golan Heights. Check out the tweet and articles referenced here: https://truthbits.blog/2019/03/23/decoding-the-golan-heights-trump-tweet/

Let’s bring in the CITIZEN CALVARY: https://truthbits.blog/2019/03/12/its-time-for-a-spring-uprising/

Have you seen our 100 Most Wanted? https://patriots4truth.org/2019/03/22/americas-most-wanted/