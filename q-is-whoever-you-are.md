---
audio: https://express.sfo2.digitaloceanspaces.com/aim/c4K5Ip1UDus.m4a
audio_duration: 00:31:52
date: 2018-05-08 02:27:31
filename: c4K5Ip1UDus.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Q is whoever you are
year: '2018'
---

Betsy and Thomas answer a reader comment in this audio. The interesting conversation that they refer to is located at the very bottom of this article in the comment section: https://patriots4truth.org/2018/03/30/were-you-conned-by-the-q-psyop/

If you want to read the Star Wars article: https://aim4truth.org/2017/02/19/source-of-the-force-secret-behind-star-wars-inspiration/