---
audio: https://express.sfo2.digitaloceanspaces.com/aim/375436812.m4a
audio_duration: 00:25:43
date: 2019-11-25 10:41:39
filename: 375436812.mp3
publisher: AIM
title: Wetware Turns Humans into Sub-Humans
year: '2019'
---

We don't want to become robot extensions for the corrupt Silicon Valley boy kings so we came up with a citizens' solution. This is not some pie-in-the-sky solution. This is something we can do to free humanity. Please educate and inform others.