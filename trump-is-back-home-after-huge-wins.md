---
audio: https://express.sfo2.digitaloceanspaces.com/aim/sP5LyZhvxFA.m4a
audio_duration: 00:40:56
date: 2018-06-13 20:05:47
filename: sP5LyZhvxFA.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Trump is Back Home After Huge Wins
year: '2018'
---

Betsy and Thomas look at the Trump tweets just as he was arriving back home after a successful trip with Kin Jong-Un. To read the tweets as you listen: https://truthbits.blog/2018/06/13/trump-is-back-home/