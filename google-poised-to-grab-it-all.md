---
audio: https://express.sfo2.digitaloceanspaces.com/aim/73ImttNVvG8.m4a
audio_duration: 00:23:15
date: 2018-10-01 21:37:18
filename: 73ImttNVvG8.m4a
playlist: AIM Insights
publisher: AIM
title: Google Poised to Grab IT All
year: '2018'
---

Michael McKibben and Douglas Gabriel continue discussing the ramifications of Google morphing into Alphabet. If you missed the first audio: https://youtu.be/M1fOmuBewhk