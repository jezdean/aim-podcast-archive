---
audio: https://express.sfo2.digitaloceanspaces.com/aim/GaSuHVngyOk.m4a
audio_duration: 00:33:11
date: 2018-09-06 15:09:06
filename: GaSuHVngyOk.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Trump Tweets Treason and Kavanaugh Explains Military Tribunals
year: '2018'
---

The headlines are exploding with talk of treason and military tribunals. Betsy and Thomas walk you through the intel so that you know the lay of the battlefield. Make sure to click on the link to pick up other intel left for you and your network of truthseekers: https://truthbits.blog/2018/09/06/treason-and-military-tribunals/