---
audio: https://express.sfo2.digitaloceanspaces.com/aim/2O-3dGCaP6w.m4a
audio_duration: 00:31:56
date: 2019-08-18 00:03:14
filename: 2O-3dGCaP6w.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Grayzone Spews Chinese Propaganda about Uighers
year: '2019'
---

To read the Grayzone article and other items discussed in the audio: https://aim4truth.org/2019/08/17/the-grayzone-posts-chinese-propaganda/