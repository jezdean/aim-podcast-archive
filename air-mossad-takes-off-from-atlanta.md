---
audio: https://express.sfo2.digitaloceanspaces.com/aim/GY_Ff8beRm8.m4a
audio_duration: 00:07:06
date: 2017-12-22 23:49:55
filename: GY_Ff8beRm8.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Air Mossad takes off from Atlanta
year: '2017'
---

What was that white cargo place that landed and took off at Hartsfield International Atlanta during the airport blackout?

See this article for more details: http://thefreethoughtproject.com/atlanta-airport-blackout-secret-plane/