---
audio: https://express.sfo2.digitaloceanspaces.com/aim/all-the-queens-men.mp3
audio_duration: 00:56:50
date: 2020-06-06 00:00:00
filename: all-the-queens-men.mp3
publisher: AIM
title: All the Queen's Men
year: '2020'
---

Betsy and Thomas discuss a wide range of topics from Lisa Page’s new job to Trump as the Highlander. All along the way, they show you how the British are still at war against freedom for the people.