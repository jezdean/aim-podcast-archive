---
audio: https://express.sfo2.digitaloceanspaces.com/aim/3nwGjMeuRpU.m4a
audio_duration: 00:38:41
date: 2017-12-05 20:58:47
filename: 3nwGjMeuRpU.m4a
playlist: Leader Tech
publisher: AIM
title: Defending the Republic. Destroying Globalism
year: '2017'
---

Douglas Gabriel from www.aim4truth.org joins Leader Technologies CEO and Founder Michael McKibben to discuss how law and order must be brought back to the Republic, especially those laws that pertain to property rights, if we are to defeat globalism and Make America Great Again.

If you don’t know who Michael McKibben is by now, please check out our six-part blockbuster interview with him https://aim4truth.org/2017/11/21/facebook-unmasked-how-the-worlds-most-relevant-entrepreneur-was-screwed-by-zuckerberg/