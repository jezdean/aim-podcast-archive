---
audio: https://express.sfo2.digitaloceanspaces.com/aim/VPQmaTvB9aQ.m4a
audio_duration: 00:35:18
date: 2017-11-21 23:19:03
filename: VPQmaTvB9aQ.m4a
playlist: Leader Tech
publisher: AIM
title: Leader Technologies Cyberwar Solution
year: '2017'
---

Full interview series at: https://aim4truth.org/2017/11/21/facebook-unmasked-how-the-worlds-most-relevant-entrepreneur-was-screwed-by-zuckerberg/

Leader Technologies’ Michael McKibben has a solution to the problem of the US military and intelligence agency’s control of the cyber-warfare experiment called the Internet. The Department of Defense created the Internet which was originally called ARPANET, after the Department of Defense agency called DARPA. These groups have incubated and funded the principle components of the Internet, from the cables it runs on, to the routers every communication runs through, to the micro-processors inside every computer, to the software that makes it work.
All areas of our modern Internet-life were created for military purposes – not to create a system of world-wide free information as Leader Technologies first envisioned. The tech-lords’ monopolies can easily be turned off through one of two plans that the shareholders of Leader Technology have devised. Both plans end the control of the Internet by governmental military and intelligence agencies. Both plans are a win-win scenario for every American. Both plans help bring America back to a lawful society where the innovations and inventions of Americans are treated with respect and not usurped for military weaponization.