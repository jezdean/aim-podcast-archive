---
audio: https://express.sfo2.digitaloceanspaces.com/aim/qgRZHf5X3nI.m4a
audio_duration: 00:49:57
date: 2019-05-22 18:06:07
filename: qgRZHf5X3nI.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Treason, Espionage, and Bill Barr
year: '2019'
---

Betsy and Thomas review the tweets of May 22, 2019 explain how Bill Barr can lock down the 2024 presidency. Read the tweets here: https://truthbits.blog/2019/05/22/bill-barr-2024/

Read about endocannabinoids here: https://patriots4truth.org/2019/05/21/endocannabinoids/