---
audio: https://express.sfo2.digitaloceanspaces.com/aim/P1Lm8KDDako.m4a
audio_duration: 00:23:40
date: 2018-07-12 16:39:06
filename: P1Lm8KDDako.m4a
playlist: AIM Insights
publisher: AIM
title: How Michael McKibben Brought Down the Iron Curtain Through Music
year: '2018'
---

Think you know all about the man who invented social media? (Sorry, to redpill you but it wasn't Dude Zuckerberg). Here is story of how he and a young group of musicians brought down the Iron Curtain of Communism. To see the original video: https://youtu.be/bE6xKGI9HOs

Scroll down to Fig. 15 for more on The Singing Revolution in the Baltics
https://www.fbcoverup.com/docs/library/Michael-T-McKibben-AFI-backgrounder.html  


To read about the amazing story of how Michael invented social media see: https://aim4truth.org/2017/11/21/facebook-unmasked-how-the-worlds-most-relevant-entrepreneur-was-screwed-by-zuckerberg/