---
audio: https://express.sfo2.digitaloceanspaces.com/aim/zRoo2hidMRI.m4a
audio_duration: 00:36:29
date: 2017-12-07 18:49:26
filename: zRoo2hidMRI.m4a
playlist: Betsy and Thomas
publisher: AIM
title: It is getting ready to hit the fan
year: '2017'
---

Betsy and Thomas discuss these topics: 

Where does Trump get his intel?

Why is the U.S. Embassy moving from Tel Aviv to Jerusalem?
Failed impeachment process by House leaves Trump’s presidency stronger

The criminal activities of Peter Strvok creates havoc and turmoil among Washington criminals

What is the backstory of Chelsea Clinton? We didn’t speak too deeply about this topic, but offer you a full report at https://aim4truth.org/2017/12/07/the-incredible-backstory-of-chelsea-clinton/ 

Paradox of Progress can be downloaded here: https://www.dni.gov/files/documents/nic/GT-Full-Report.pdf