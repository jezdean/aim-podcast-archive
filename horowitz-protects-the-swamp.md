---
audio: https://express.sfo2.digitaloceanspaces.com/aim/1F1ZlXM1Oqg.m4a
audio_duration: 00:22:05
date: 2018-06-15 02:13:52
filename: 1F1ZlXM1Oqg.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Horowitz Protects the Swamp
year: '2018'
---

Click here to see what we were telling our listeners in December 2017 about Horowitz: https://truthbits.blog/2018/06/15/horowitz-protects-the-swamp/