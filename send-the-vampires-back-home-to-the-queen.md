---
audio: https://express.sfo2.digitaloceanspaces.com/aim/j1suf5AAtC8.m4a
audio_duration: 00:49:15
date: 2018-04-27 16:53:18
filename: j1suf5AAtC8.m4a
playlist: AIM Insights
publisher: AIM
title: Send the vampires back home to the Queen
year: '2018'
---

Douglas Gabriel (Thomas Paine) and Michael McKibben discuss their latest findings from their research. Learn how the America you were taught in school is not the America that is actually operating. Nothing so refreshing than nourishing your thinking with truth. We can’t get enough of it. How about you?

Here are some articles that may interest you: https://americans4innovation.blogspot.com/2018/04/the-shadow-government-uses-ses-serco.html

https://aim4truth.org/2018/04/24/hop-on-board-the-opic-usaid-corporate-gravy-train-operated-by-senior-executive-service-fuel-by-u-s-taxpayers/

https://aim4truth.org/2018/04/17/exposed-all-the-queens-agents-and-corporations-that-control-the-world/ 

Here is a PDF chart of how the United States is really controlled by foreign forces: https://www.fbcoverup.com/docs/library/2018-04-25-Constitution-vs-Crown-and-Deep-State-Apr-25-2018.pdf