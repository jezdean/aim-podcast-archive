---
audio: https://express.sfo2.digitaloceanspaces.com/aim/8Ol9x4da5GE.m4a
audio_duration: 00:34:15
date: 2018-01-28 19:23:36
filename: 8Ol9x4da5GE.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Dragnet in the Swamp
year: '2018'
---

The letter from Senator Chuck Grassley is like a huge net cast out upon the swamp, ready to pull in the first net of swamp creatures. Listen to Thomas review the list with Betsy. 

To see the letter, click here: https://theconservativetreehouse.com/2018/01/26/on-notice-senator-chuck-grassley-sends-letters-requesting-information/

To read about how Obama used the U.S. Digital Services to rig the election, click here: https://aim4truth.org/2017/11/24/absolute-proof-obama-not-russians-rigged-elections/