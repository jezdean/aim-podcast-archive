---
audio: https://express.sfo2.digitaloceanspaces.com/aim/x8s7mW1eCMU.m4a
audio_duration: 00:44:50
date: 2018-10-15 17:54:29
filename: x8s7mW1eCMU.m4a
playlist: Betsy and Thomas
publisher: AIM
title: The swamp is hanging by a thread
year: '2018'
---

Betsy asks Thomas about Mad Dog Mattis, Nikki Haley, George Papadopoulos, James Baker, and the Pope. Continue your education here: https://truthbits.blog/2018/10/15/the-swamp-is-handing-by-a-thread/