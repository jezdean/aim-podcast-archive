---
audio: https://express.sfo2.digitaloceanspaces.com/aim/9W4vkoh5zeI.m4a
audio_duration: 00:41:38
date: 2018-09-01 19:08:40
filename: 9W4vkoh5zeI.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Canada Caught Red Handed
year: '2018'
---

Betsy and Thomas decode the Trump Tweets of September 1, 2018. Trump calls out the fake news and the horrible NAFTA deal that is now leaving Canada with no deal. To read the tweets and the other information mentioned in the audio: https://truthbits.blog/2018/09/01/trump-to-canada-make-a-deal-or-go-back-to-pre-nafta/