---
audio: https://express.sfo2.digitaloceanspaces.com/aim/Q6LgR0lAMGs.m4a
audio_duration: 00:29:54
date: 2019-08-23 23:43:02
filename: Q6LgR0lAMGs.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Patrick Byrne Gets Redpilled by a Russian Spy
year: '2019'
---

Betsy and Thomas discuss the resignation of Patrick Byrne from his CEO position at Overstock and his relationship with Russian spy Maria Butina.

Here is a recommended article that will help you get up to speed on the situation and explain how Peter Strzok, Andrew McCabe, and James Comey are center stage again.  
https://truthbits.blog/2019/08/23/patrick-byrne-gets-redpilled-by-a-russian-spy/