---
audio: https://express.sfo2.digitaloceanspaces.com/aim/srS9ajz-S38.m4a
audio_duration: 00:06:48
date: 2018-03-28 00:55:48
filename: srS9ajz-S38.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Kitty likes sunshine
year: '2018'
---

The best disinfectant is the pure light of sunshine. Trolls turn into stone in the sunlight. Once you see the effects of sunlight, you will want all of your puppies and kitties get plenty.

All smithies and miners are welcomed to take our video and replicate on your channel...although people would probably like a different picture on the thumbnail.