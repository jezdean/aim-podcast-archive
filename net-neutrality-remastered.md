---
audio: https://express.sfo2.digitaloceanspaces.com/aim/47UOt6lTKuQ.m4a
audio_duration: 00:26:07
date: 2017-12-02 12:40:51
filename: 47UOt6lTKuQ.m4a
playlist: Leader Tech
publisher: AIM
title: Net Neutrality REMASTERED
year: '2017'
---

VOLUME: THIS IS A DUPLICATE VIDEO BUT WITH INCREASED SOUND.

Douglas Gabriel of www.aim4truth.org Michael McKibben of Leader Technologies https://aim4truth.org/2017/11/21/facebook-unmasked-how-the-worlds-most-relevant-entrepreneur-was-screwed-by-zuckerberg/ discuss the following:

• ARPANET and the creation of the Internet
• The myth of Sir Tim Berners-Lee and the World Wide Web – CERN
• Steven Crocker and the ICANN give-away to the United Nations
• Who owns the Internet? – its cables, its routers, its cloud computing, cell-towers, etc.
• The Myth of a “Russian Internet” being created
• China, Turkey, Syria, Iran, etc. can all “turn off” the Internet or any part of it
• Why does the US government play like they didn’t invent, regulate, and effectively own the Internet?
• Federal Communication Commission pays AT&T, Verizon and other companies Billions to install the lines
• Facebook/Google/Twitter and other social media surveil and control all user data and target them for marketing manipulation and US propaganda
• The Internet is a weaponized DARPA experiment that got out of control