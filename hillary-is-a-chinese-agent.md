---
audio: https://express.sfo2.digitaloceanspaces.com/aim/36eK_8PLCyI.m4a
audio_duration: 00:29:27
date: 2019-08-16 20:24:31
filename: 36eK_8PLCyI.m4a
playlist: AIM Insights
publisher: AIM
title: Hillary is a Chinese Agent
year: '2019'
---

Douglas Gabriel and Michael McKibben examine the Grassley - Johnson Investigation of the DOJ’s and FBI’s Handling of the Clinton Investigation. 

To access the team's research, please go here and share with your circle of influence: https://patriots4truth.org/2019/08/16/hillary-is-a-chinese-agent/