---
audio: https://express.sfo2.digitaloceanspaces.com/aim/PhXAbbOpS44.m4a
audio_duration: 00:27:14
date: 2018-06-21 22:11:07
filename: PhXAbbOpS44.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Full House is a Strong Hand to Play
year: '2018'
---

Betsy and Thomas decode important Trump Tweets of June 20, 2018. Listen and learn. It is news if you hear it today, but history you need to know tomorrow. To see the support material that was referenced: https://truthbits.blog/2018/06/21/a-full-house-is-a-strong-hand-to-play/