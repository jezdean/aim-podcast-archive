---
audio: https://express.sfo2.digitaloceanspaces.com/aim/Q339zXobyqc.m4a
audio_duration: 00:23:41
date: 2019-10-28 16:06:22
filename: Q339zXobyqc.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Douglas Gabriel gets out of the weeds, into the stars
year: '2019'
---

Learn more about meteorites, THC cannabis, hemp, shungite in this link: https://truthbits.blog/2019/10/27/cosmic-thc-found-on-meteorites/