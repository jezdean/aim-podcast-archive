---
audio: https://express.sfo2.digitaloceanspaces.com/aim/IXm24MjooFs.m4a
audio_duration: 00:33:19
date: 2019-07-03 01:26:53
filename: IXm24MjooFs.m4a
playlist: AIM Insights
publisher: AIM
title: Former Lover Exposes Eric Schmidt
year: '2019'
---

Betsy and Thomas read a transcript of an interview with a Google/Facebook/DARPA insider that was conducted with a member of the Anonymous Patriots, a citizen journalist group aligned with the American Intelligence Media. The person interviewed wishes to remain anonymous and for purposes of the interview will be called Jane Doe. This conversation took place on July 1st, 2019.

To read a transcript of the interview: https://aim4truth.org/2019/07/02/former-lover-exposes-eric-schmidt/