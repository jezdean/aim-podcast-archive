---
audio: https://express.sfo2.digitaloceanspaces.com/aim/386526403.m4a
audio_duration: 00:26:19
date: 2020-01-22 12:05:07
filename: 386526403.mp3
publisher: AIM
title: Day Two – Senate Impeachment Trial - Betsy and Thomas Discuss
year: '2020'
---

If you missed Day One Senate Impeachment Trial commentary, click here: truthbits.blog/2020/01/22/day-one-senate-impeachment-trial/