---
audio: https://express.sfo2.digitaloceanspaces.com/aim/ZFrmNX_Gwik.m4a
audio_duration: 00:25:20
date: 2018-05-11 21:21:32
filename: ZFrmNX_Gwik.m4a
playlist: Betsy and Thomas
publisher: AIM
title: They're up the swamp without a paddle
year: '2018'
---

What was the redacted name that Devin Nunes and SES Trey Gowdy went to look at in the redacted files of the DOJ? Sergei Millian Poroshenko, Stefan Halper, Carter Page, George Papadopoulos?