---
audio: https://express.sfo2.digitaloceanspaces.com/aim/XMQBDuTVsE4.m4a
audio_duration: 00:53:42
date: 2018-05-19 20:26:43
filename: XMQBDuTVsE4.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Moles Unplug Hole in the Swamp
year: '2018'
---

You will not want to miss this Betsy and Thomas discussion where Thomas goes so far out in the weeds that he finds the exact mole that unplugs the swamp.