---
audio: https://express.sfo2.digitaloceanspaces.com/aim/j6qIfobQbL8.m4a
audio_duration: 00:36:22
date: 2018-04-13 20:04:15
filename: j6qIfobQbL8.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Back in the Swamp
year: '2018'
---

Lyin Comey hits the book trail; more Senior Executive Service uncovered; SERCO runs our country. Join Betsy and Thomas for a look in the swamp.