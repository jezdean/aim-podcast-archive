---
audio: https://express.sfo2.digitaloceanspaces.com/aim/18_YHndyw44.m4a
audio_duration: 00:29:39
date: 2018-08-19 23:47:32
filename: 18_YHndyw44.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Fake News and the Witch Hunt
year: '2018'
---

Betsy and Thomas decode Trump’s tweets of August 18 and 19, 2018. In this episode, POTUS calls out the New York Times for outright fake news stories as he continues to hammer SES operatives – Brennan, Comey, McCabe, Strzok, etc. To read the tweets as you listen and see other interesting information: https://truthbits.blog/2018/08/19/fake-news-and-the-witch-hunt/