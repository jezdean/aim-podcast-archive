---
audio: https://express.sfo2.digitaloceanspaces.com/aim/YEWYj40-RUY.m4a
audio_duration: 00:30:15
date: 2018-07-22 01:41:04
filename: YEWYj40-RUY.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Donald Trump is Today's Highlander MacLeod
year: '2018'
---

Betsy and Thomas decode Trump’s tweets of July 21, 2018. Listen to Thomas explain why Trump is like the Highlander MacLeod in more ways than you might think and learn something about the mystery of the Spear of Destiny.  To read the tweets as you listen and see other interesting information: https://truthbits.blog/2018/07/22/trump-is-todays-highlander-macleod/