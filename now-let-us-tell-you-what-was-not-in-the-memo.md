---
audio: https://express.sfo2.digitaloceanspaces.com/aim/7mE0xu8ECXA.m4a
audio_duration: 00:50:09
date: 2018-02-03 01:03:25
filename: 7mE0xu8ECXA.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Now….let us tell you what was NOT in the memo.
year: '2018'
---

Here is the second part to this audio: https://www.youtube.com/watch?v=y4bSgRET7oM