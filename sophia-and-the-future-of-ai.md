---
audio: https://express.sfo2.digitaloceanspaces.com/aim/rBPI6z5X_w0.m4a
audio_duration: 00:40:27
date: 2017-12-11 18:05:20
filename: rBPI6z5X_w0.m4a
playlist: AIM Insights
publisher: AIM
title: Sophia and the Future of AI
year: '2017'
---

As our regular listeners know, the American Intelligence is more than an information channel, and it is certainly not an entertainment channel—just look at our graphics and hastily uploaded audios! We provide our listeners with INTELLIGENCE. We show you the nuggets of information on the INFOWAR battlefield and arrange them in a way so that you can comprehend what you are seeing. 

We try to lift the fog of war so that you have a better view of the information terrain than our enemies do. We are intelligence operatives that are crowdsourcing intelligence to our fellow Americans since our lying corrupt media is incapable of doing so. 

In this endeavor to provide you with excellent intelligence, our Conclave group meets to discuss important data and decipher their codes. In this audio, three of our members meet to discuss the future of AI. 

Please enjoy the discussion with Douglas Gabriel, Michael McKibben, and John Barnwell.