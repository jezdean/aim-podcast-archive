---
audio: https://express.sfo2.digitaloceanspaces.com/aim/VyiioZ9owh8.m4a
audio_duration: 00:26:04
date: 2019-02-01 19:01:27
filename: VyiioZ9owh8.m4a
playlist: Betsy and Thomas
publisher: AIM
title: No one goes to jail in the swamp because it is legal to lie
year: '2019'
---

Tyla and Douglas Gabriel are joined with Michael McKibben to explain how it is now perfectly legal to lie to Congress. Anyone can do it and we show you a 1996 law they passed while you were “asleep” to make sure that no one goes to jail.  Make sure to view the laws yourself here: https://patriots4truth.org/2019/02/01/it-is-legal-to-lie-to-congress/