---
audio: https://express.sfo2.digitaloceanspaces.com/aim/mIVeZ1yD6PM.m4a
audio_duration: 00:32:54
date: 2017-12-31 18:11:01
filename: mIVeZ1yD6PM.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Freedom Trackers for 'Free Range' Criminals
year: '2017'
---

Betsy and Thomas think the FREEDOM TRACKER boot is a great way to identify the criminals and let them experience a “free range” prison where they have to pay for their own food and lodging instead of Vanguard private prisons pocketing the profits for their upkeep. The Freedom Tracker boot allows the criminals to be identified anywhere – on the streets, at the mall, in the grocery store – but citizens don’t have to pick up their incarceration costs. 
The geopolitical Sherpas also take on questions from readers:

8:50 What’s up with Leo Wanta
12:05 Barrick Gold and Peter Munk 
25:00 Iran’s people-led revolution

Articles that were mentioned: 
https://aim4truth.org/2017/12/17/doj-inspector-general-michael-horowitz-was-handpicked-by-soros-co/

https://aim4truth.org/2017/12/29/corporate-transnational-warlord-pirates-are-on-the-run/

https://aim4truth.org/2017/11/21/facebook-unmasked-how-the-worlds-most-relevant-entrepreneur-was-screwed-by-zuckerberg/