---
audio: https://express.sfo2.digitaloceanspaces.com/aim/U0nCx6eMNdw.m4a
audio_duration: 00:52:29
date: 2018-07-17 02:53:06
filename: U0nCx6eMNdw.m4a
playlist: Betsy and Thomas
publisher: AIM
title: World Peace is Happening!!!
year: '2018'
---

Betsy and Thomas decode Trump’s tweets of July 16, 2018. These tweets capture the excitement of the Helsinki Summit. To read the tweets as you listen and see other interesting information: https://truthbits.blog/2018/07/17/trump-and-putin-collude-for-world-peace/