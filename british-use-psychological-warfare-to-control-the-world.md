---
audio: https://express.sfo2.digitaloceanspaces.com/aim/PfbwONHcSQ8.m4a
audio_duration: 00:56:27
date: 2019-01-09 21:07:24
filename: PfbwONHcSQ8.m4a
playlist: AIM Insights
publisher: AIM
title: British Use Psychological Warfare to Control the World
year: '2019'
---

Douglas Gabriel and Michael McKibben continue to unwind the Gordian Knot of British propaganda that has been going on for over 100 years. Once you see how they have done it, using psyops from the Tavistock Institute, you will never see the world the same way again. After listening to this riveting discussion, follow this link and learn more: https://patriots4truth.org/2019/01/09/the-british-use-psychological-warfare-to-control-the-world/