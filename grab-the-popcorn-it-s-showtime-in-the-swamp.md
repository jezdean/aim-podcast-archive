---
audio: https://express.sfo2.digitaloceanspaces.com/aim/3clI6v1P8SQ.m4a
audio_duration: 00:28:43
date: 2018-10-18 20:22:37
filename: 3clI6v1P8SQ.m4a
playlist: Betsy and Thomas
publisher: AIM
title: GRAB THE POPCORN! It's showtime in the swamp
year: '2018'
---

Continue your citizen education here: https://truthbits.blog/2018/10/18/its-showtime-in-the-swamp-grab-the-popcorn/