---
audio: https://express.sfo2.digitaloceanspaces.com/aim/6xju4BR2HWw.m4a
audio_duration: 00:32:03
date: 2019-08-01 17:17:10
filename: 6xju4BR2HWw.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Time to Play the Trump Card to Save the World
year: '2019'
---

Let President Trump know that you stand with him: https://www.whitehouse.gov/contact/

Show your support of Betsy Ross and the world wide freedom movement: https://rushlimbaughshowstore.com/products/stand-up-for-betsy-ross-t-white