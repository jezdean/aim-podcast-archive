---
audio: https://express.sfo2.digitaloceanspaces.com/aim/TCzi7efJDVk.m4a
audio_duration: 00:24:47
date: 2019-02-26 14:45:06
filename: TCzi7efJDVk.m4a
playlist: AIM Insights
publisher: AIM
title: Harry Potter and Tavistock Brainwashing
year: '2019'
---

Think you know everything about Tavistock, JK Rowling, and Harry Potter? Listen to the two smartest guys in the room have a discussion about what they found out.