---
audio: https://express.sfo2.digitaloceanspaces.com/aim/hO2-Yrctnt8.m4a
audio_duration: 00:25:44
date: 2018-06-12 17:01:05
filename: hO2-Yrctnt8.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Peace is the Prize
year: '2018'
---

Betsy and Thomas are celebrating with coffee and cheesecake today on Trump’s BIG WIN in Korea. Step in and join the conversation – which may be digital, but always insightful. 

Here is Douglas' backstory on Star Wars: https://aim4truth.org/2017/02/19/source-of-the-force-secret-behind-star-wars-inspiration/