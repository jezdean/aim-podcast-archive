---
audio: https://express.sfo2.digitaloceanspaces.com/aim/SqVkZV2Qrc4.m4a
audio_duration: 00:41:17
date: 2018-06-29 19:01:02
filename: SqVkZV2Qrc4.m4a
playlist: AIM Insights
publisher: AIM
title: Queen Elizabeth II Rigs Elections Around the World
year: '2018'
---

Douglas Gabriel and Michael McKibben explain how the British Monarch, Queen Elizabeth II, and her Privy Council rig elections all around the world. Astonishing, jaw-dropping intel. Then to read more see this link: https://patriots4truth.org/2018/06/29/queen-elizabeth-ii-rigs-elections-worldwide/