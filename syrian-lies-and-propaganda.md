---
audio: https://express.sfo2.digitaloceanspaces.com/aim/hNkKYs-7bKU.m4a
audio_duration: 00:25:28
date: 2018-04-13 19:37:47
filename: hNkKYs-7bKU.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Syrian Lies and Propaganda
year: '2018'
---

Betsy and Thomas review the Syrian situation and the many forces involved.