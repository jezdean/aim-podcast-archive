---
audio: https://express.sfo2.digitaloceanspaces.com/aim/MlhexVuNVeg.m4a
audio_duration: 00:21:48
date: 2018-05-03 21:04:34
filename: MlhexVuNVeg.m4a
playlist: AIM Insights
publisher: AIM
title: Who owns the Golden Share and why does it matter?
year: '2018'
---

Douglas Gabriel and Michael McKibben of Americans explain what the Golden Share is. If you want to understand how the world really operates, then stop in and listen to these two brilliant researchers explain the connections of SES, Serco, the Crown Agents, and Queen Lizzie.

See this article for more: https://aim4truth.org/2018/04/17/exposed-all-the-queens-agents-and-corporations-that-control-the-world/