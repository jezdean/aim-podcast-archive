---
audio: https://express.sfo2.digitaloceanspaces.com/aim/ChpEC0yakKk.m4a
audio_duration: 00:28:39
date: 2018-06-20 23:52:12
filename: ChpEC0yakKk.m4a
playlist: Betsy and Thomas
publisher: AIM
title: New Alliance of Nations is in the Making
year: '2018'
---

Betsy and Thomas decode only one tweet in this audio and it is a doozy. Do you really know what’s going on with Trump and King Felipe VI? To see the support material that was referenced: https://truthbits.blog/2018/06/20/alliance-between-the-u-s-and-spain-is-being-renewed/