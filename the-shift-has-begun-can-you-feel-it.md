---
audio: https://express.sfo2.digitaloceanspaces.com/aim/wwXRXWalSYI.m4a
audio_duration: 00:12:54
date: 2018-06-02 21:01:17
filename: wwXRXWalSYI.m4a
playlist: Betsy and Thomas
publisher: AIM
title: The Shift has begun. Can you feel it?
year: '2018'
---

Nina and Betsy (Tyla) see the consciousness of the people of the Earth coming into alignment with truth and love.  This monumental shift of awareness unfolds as rose petals blossoming with pink scents in the air. To see all of Nina’s beautiful pieces go to: http://www.ninahowardstudio.com/