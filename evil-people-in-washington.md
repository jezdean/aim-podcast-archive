---
audio: https://express.sfo2.digitaloceanspaces.com/aim/2yDPSJQG-wI.m4a
audio_duration: 00:34:02
date: 2018-08-03 00:40:42
filename: 2yDPSJQG-wI.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Evil People in Washington
year: '2018'
---

After reading an article from the Gateway Pundit (see link), Thomas and Betsy discussed whether things are as desperate as the publication makes it seem. Here is the link: https://truthbits.blog/2018/08/03/a-lot-of-bad-people-in-washington/