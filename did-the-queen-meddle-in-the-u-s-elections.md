---
audio: https://express.sfo2.digitaloceanspaces.com/aim/6IqlBm_O85A.m4a
audio_duration: 00:50:59
date: 2018-07-11 16:13:48
filename: 6IqlBm_O85A.m4a
playlist: AIM Insights
publisher: AIM
title: Did the Queen meddle in the U S  elections?
year: '2018'
---

Michael McKibben joins Douglas Gabriel to explain the details of how Queen Elizabeth II and her Privy Council meddled in U.S. elections and attempted an overthrow of Donald J. Trump.