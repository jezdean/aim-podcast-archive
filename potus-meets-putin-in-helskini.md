---
audio: https://express.sfo2.digitaloceanspaces.com/aim/IFuEHLR79FY.m4a
audio_duration: 00:33:47
date: 2018-07-15 22:17:38
filename: IFuEHLR79FY.m4a
playlist: Betsy and Thomas
publisher: AIM
title: POTUS meets Putin in Helskini
year: '2018'
---

Betsy and Thomas decode Trump’s tweets of July 15, 2018. Will we see world peace when Trump and Putin get together?  What are we going to do with cyber security? To read the tweets as you listen and see other interesting information: https://truthbits.blog/2018/07/15/potus-meets-putin/