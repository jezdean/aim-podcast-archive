---
audio: https://express.sfo2.digitaloceanspaces.com/aim/zq2CsLQq0TM.m4a
audio_duration: 00:34:15
date: 2018-06-02 19:41:05
filename: zq2CsLQq0TM.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Here come the June tweets
year: '2018'
---

Betsy and Thomas review Trump Tweets of May 31 – June 2, 2018. Trump tweets point to John Brennan as the biggest liar ever in American history. Follow along at this link which is also very easy to pass along in your social media network. https://truthbits.blog/2018/06/02/decoding-trump-tweets-may-31-june-2-2018/

Bypass YouTube altogether and follow www.truthbits.blog to receive Trump Tweet decodes as they are posted. Audio and tweets all on one page. Plus, special surprises at the bottom of the page.