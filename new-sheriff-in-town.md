---
audio: https://express.sfo2.digitaloceanspaces.com/aim/7RuGRZNoUy4.m4a
audio_duration: 00:34:06
date: 2018-02-14 00:10:10
filename: 7RuGRZNoUy4.m4a
playlist: Betsy and Thomas
publisher: AIM
title: New Sheriff in Town
year: '2018'
---

You are going to be blown away with the revelations that Thomas has for you in this deep dive.  He is unleashed, way out in the weeds, bringing up the biggest swamp monster of all, handing it to We the People to give it to President Trump.  

For more on Susan Rice see this citizens intelligence report: https://aim4truth.org/2017/04/16/susan-rice-unmasked-white-house-warmonger/