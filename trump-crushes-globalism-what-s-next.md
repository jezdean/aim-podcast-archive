---
audio: https://express.sfo2.digitaloceanspaces.com/aim/cm0A7uIFpQc.m4a
audio_duration: 00:51:56
date: 2019-06-04 17:37:04
filename: cm0A7uIFpQc.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Trump crushes globalism. What's next?
year: '2019'
---

Trump is crushing globalism, but you wouldn’t know it if you listened to fake Fox News. Let Betsy and Thomas explain the wins. Click here to look at the other reference material: https://truthbits.blog/2019/06/04/trump-is-crushing-globalism/