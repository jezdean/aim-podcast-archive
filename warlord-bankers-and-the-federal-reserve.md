---
audio: https://express.sfo2.digitaloceanspaces.com/aim/LvYRrLVAI7A.m4a
audio_duration: 00:22:11
date: 2019-03-30 21:32:29
filename: LvYRrLVAI7A.m4a
playlist: History of Banking
publisher: AIM
title: Warlord Bankers and the Federal Reserve
year: '2019'
---

Follow our banking series as we take you step by step how easy it is to close the Federal Reserve. We are building an interactive e-book and this is the first lesson. 

Click here to see the entire lesson: https://truthbits.blog/2019/03/30/warlord-bankers-and-the-federal-reserve/

Access the full lesson on banking with all audios here: https://aim4truth.org/2019/04/03/god-or-mammon/