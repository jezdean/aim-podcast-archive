---
audio: https://express.sfo2.digitaloceanspaces.com/aim/VqrF37tXxTc.m4a
audio_duration: 00:50:18
date: 2018-10-09 18:01:11
filename: VqrF37tXxTc.m4a
playlist: Betsy and Thomas
publisher: AIM
title: These MAGA cats are ready for action
year: '2018'
---

Betsy and Thomas run through 3 current events topics:
1) Rod Rosenstein’s in-flight meeting with POTUS
2) Oleg Deripaska-Diane Feinstein-Fusion GPS-Daniel Jones web of deceit
3) China-Israel connection, Talpiot, Unit 8200