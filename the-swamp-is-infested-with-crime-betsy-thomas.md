---
audio: https://express.sfo2.digitaloceanspaces.com/aim/380797473.m4a
audio_duration: 00:57:21
date: 2019-12-20 15:04:07
filename: 380797473.mp3
publisher: AIM
title: The swamp is infested with crime - Betsy & Thomas
year: '2019'
---

Betsy and Thomas discuss a wide array of topics which can be found with sourced materials, headlines, and videos on the day's Cat Report: aim4truth.org/2019/12/20/cat-report-245/ Among topics that you might not expect are their discussions on the coming mini-ice age, the magnetic north pole shift, and how mitochondria responds to CBDs.