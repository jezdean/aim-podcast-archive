---
audio: https://express.sfo2.digitaloceanspaces.com/aim/AddUKu9CIXM.m4a
audio_duration: 00:45:15
date: 2018-11-17 20:59:48
filename: AddUKu9CIXM.m4a
playlist: AIM Insights
publisher: AIM
title: Eyewitness barely makes it out alive in California fire storm
year: '2018'
---

Douglas Gabriel interviews Robert Otey in his first-hand account of what it was like on the ground as the California firestorm broke out.  After listening to this video, you may want to help Robert. His website is www.feandft.com . His paypal account is robert@feandft.com.

He will be returning to his homestead after the fire and will be uploading videos on his YouTube channel at https://www.youtube.com/channel/UCzeVAiRb7Fld_ztsebxS55g 

Learn more here: https://truthbits.blog/2018/11/17/eyewitness-barely-makes-it-out-alive-in-california-fire-storm/