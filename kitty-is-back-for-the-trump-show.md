---
audio: https://express.sfo2.digitaloceanspaces.com/aim/kM0kEb7yeEU.m4a
audio_duration: 00:36:53
date: 2018-09-18 23:24:12
filename: kM0kEb7yeEU.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Kitty is back for the Trump show
year: '2018'
---

Betsy and Thomas review the trump tweets of September 18, 2018. They also give some insight into the observatories that were closed last week and an update on the war with China. To read the tweets and see other info: https://truthbits.blog/2018/09/18/its-showtime-for-the-swamp/