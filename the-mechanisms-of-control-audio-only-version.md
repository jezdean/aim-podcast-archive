---
audio: https://express.sfo2.digitaloceanspaces.com/aim/382245227.m4a
audio_duration: 01:09:25
date: 2019-12-31 20:09:11
filename: 382245227.mp3
publisher: AIM
title: The Mechanisms of Control [Audio only version]
year: '2019'
---

Enjoy our last video of the year, a conversation between Douglas and Michael that is absolutely HISTORIC!!. We put it at the top of this month's collection of videos. Enjoy. aim4truth.org/2019/12/31/aim-videos-audios/