---
audio: https://express.sfo2.digitaloceanspaces.com/aim/MMwejAXN6PY.m4a
audio_duration: 00:30:14
date: 2019-04-24 19:03:24
filename: MMwejAXN6PY.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Trump tweets about UK Intelligence
year: '2019'
---

Betsy and Thomas decode Trump tweets of April 23 and 24, 2019 where Trump points to British intelligence for helping Obama spy and him. 

Read the tweets and other material here: https://truthbits.blog/2019/04/24/cats-out-of-the-bag-trump-tweets-about-uk-intelligence/