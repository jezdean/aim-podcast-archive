---
audio: https://express.sfo2.digitaloceanspaces.com/aim/SIWQut9NPBg.m4a
audio_duration: 00:42:04
date: 2018-08-28 20:30:00
filename: SIWQut9NPBg.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Google is suppressing Conservative voices
year: '2018'
---

Betsy and Thomas decode Trump’s tweets of August 28, 2018, taking the listener on a whirlwind discussion starting with minority winning, to NASDAQ gains, trade deals, consumer confidence, adding a dash of McCain and Pope Francis corruption.  To read the tweets as you listen and read more in-depth articles: https://truthbits.blog/2018/08/28/consumer-confidence-highest-level-since-october-2000/