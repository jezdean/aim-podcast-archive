---
audio: https://express.sfo2.digitaloceanspaces.com/aim/aEiibIYjNc0.m4a
audio_duration: 00:28:04
date: 2019-03-24 21:32:51
filename: aEiibIYjNc0.m4a
playlist: Betsy and Thomas
publisher: AIM
title: No Collusion  No Obstruction
year: '2019'
---

Betsy and Thomas review AG Bill Barr’s letter regarding the Mueller Report. Read the original letter here: https://judiciary.house.gov/sites/democrats.judiciary.house.gov/files/documents/AG%20March%2024%202019%20Letter%20to%20House%20and%20Senate%20Judiciary%20Committees.pdf

Let’s bring in the CITIZEN CALVARY: https://truthbits.blog/2019/03/12/its-time-for-a-spring-uprising/

Have you seen our 100 Most Wanted? https://patriots4truth.org/2019/03/22/americas-most-wanted/