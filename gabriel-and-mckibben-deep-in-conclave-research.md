---
audio: https://express.sfo2.digitaloceanspaces.com/aim/l0rvFGmRkFA.m4a
audio_duration: 00:36:39
date: 2018-10-17 03:26:00
filename: l0rvFGmRkFA.m4a
playlist: AIM Insights
publisher: AIM
title: Gabriel and McKibben Deep in Conclave Research
year: '2018'
---

Douglas and Michael are the top dogs in the Conclave. In this video, the two show off their research skills, a very important skill needed in fighting the Great Information War.