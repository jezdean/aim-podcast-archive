---
audio: https://express.sfo2.digitaloceanspaces.com/aim/2yHInp2TRCY.m4a
audio_duration: 00:45:22
date: 2019-04-06 15:42:55
filename: 2yHInp2TRCY.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Educating George Papadopoulos
year: '2019'
---

Are you confused about what is going on at the border? Betsy and Thomas explain while they decode Trump tweets of April 5, 2019. Read the tweets and see the article Thomas recommends on USMCA: https://truthbits.blog/2019/04/06/educating-george-papadopoulos/