---
audio: https://express.sfo2.digitaloceanspaces.com/aim/dD2NFlA2M-4.m4a
audio_duration: 00:21:48
date: 2018-04-01 18:54:20
filename: dD2NFlA2M-4.m4a
playlist: Betsy and Thomas
publisher: AIM
title: SES Awareness Month
year: '2018'
---

Important reference materials: https://patriots4truth.org/2018/04/01/april-is-senior-executive-services-awareness-month/