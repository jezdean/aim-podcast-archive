---
audio: https://express.sfo2.digitaloceanspaces.com/aim/meBhj0WF83c.m4a
audio_duration: 01:26:28
date: 2019-06-21 18:52:57
filename: meBhj0WF83c.m4a
playlist: AIM Insights
publisher: AIM
title: Zagami and Gabriel Expose Vatican Evil
year: '2019'
---

Leo Zagami discusses current issues in the Vatican with Douglas Gabriel and John Barnwell.