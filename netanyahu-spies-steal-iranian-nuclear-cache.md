---
audio: https://express.sfo2.digitaloceanspaces.com/aim/eJ-SgrfKevQ.m4a
audio_duration: 00:29:07
date: 2018-04-30 20:44:49
filename: eJ-SgrfKevQ.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Netanyahu Spies Steal Iranian Nuclear Cache
year: '2018'
---

Betsy and Thomas discuss Netanyahu’s big reveal that Iranians want nuclear weapons. 

See his ‘Ted Talk’ here. https://www.youtube.com/watch?v=_qBt4tSCALA&feature=youtu.be