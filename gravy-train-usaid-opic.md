---
audio: https://express.sfo2.digitaloceanspaces.com/aim/bAt7L05gOD0.m4a
audio_duration: 00:32:42
date: 2018-04-24 20:42:22
filename: bAt7L05gOD0.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Gravy train USAID - OPIC
year: '2018'
---

Betsy and Thomas explain their most recent article on more scams by foreign aid agencies run against Americans. Here is the article: 

https://aim4truth.org/2018/04/24/hop-on-board-the-opic-usaid-corporate-gravy-train-operated-by-senior-executive-service-fuel-by-u-s-taxpayers/