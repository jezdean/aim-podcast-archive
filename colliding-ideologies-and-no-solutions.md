---
audio: https://express.sfo2.digitaloceanspaces.com/aim/nk1UTMUrUNg.m4a
audio_duration: 00:20:32
date: 2018-05-15 18:10:44
filename: nk1UTMUrUNg.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Colliding Ideologies and No Solutions
year: '2018'
---

Betsy and Thomas look at the middle east and the colliding ideologies that have been forever at war so why don’t we get out and let them fight it out among themselves.