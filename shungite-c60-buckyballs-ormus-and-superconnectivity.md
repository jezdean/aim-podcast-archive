---
audio: https://express.sfo2.digitaloceanspaces.com/aim/wFYrAmt4gEc.m4a
audio_duration: 00:44:23
date: 2018-02-04 19:33:29
filename: wFYrAmt4gEc.m4a
playlist: AIM Insights
publisher: AIM
title: Shungite, C60, Buckyballs, Ormus, and Superconnectivity
year: '2018'
---

Thomas Paine and John Barnwell discuss some great ways to antidote 5G. Listen as they explore ormus, C60, shungite, buckyballs, and superconnectivity.

Betsy and Thomas get many questions about the protocols they use. This short audio https://www.youtube.com/watch?v=ru0rR_HV6Ms is a way to provide you these links on our YouTube channel.

We have several articles on 5g. Here is a good one to get started: https://patriots4truth.org/2018/01/31/how-to-antidote-5g/

This is an audio that we did on shungite and 5G: https://www.youtube.com/watch?v=wFYrAmt4gEc&feature=youtu.be

To learn about Paul’s devices for EMF and 5G protection see: www.basicstowellness.com

To access the ASCEND diet: http://www.ourspirit.com/ascension-diet

Here is our self-assessment symptom list: http://www.ourspirit.com/symptom-list

This is our special formula enzyme called Vibes: http://www.ourspirit.com/vibe-enzymes 

Of course we have a medical disclaimer: http://www.ourspirit.com/medical-disclaimer