---
audio: https://express.sfo2.digitaloceanspaces.com/aim/cy4zpeHZL8g.m4a
audio_duration: 00:13:19
date: 2018-04-29 19:33:37
filename: cy4zpeHZL8g.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Is 5G making you fat?
year: '2018'
---

Since we don’t know what the effects of 4G and 5G on the human population, Betsy and Thomas speculate whether this could be another reason for the rising rates of obesity and over weightness in the world. What do you think?

Here are some of our suggestions for antidoting 5G: https://patriots4truth.org/2018/01/31/how-to-antidote-5g/

We are really big on enzymes. Check this link out to learn more: http://www.ourspirit.com/vibe-enzymes

These are the enzymes that we take daily (only available for U.S. shipping): http://www.ourspirit.com/copy-of-vibes-digestive-enzyme