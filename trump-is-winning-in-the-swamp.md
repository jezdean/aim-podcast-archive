---
audio: https://express.sfo2.digitaloceanspaces.com/aim/7xPI9UQVvOE.m4a
audio_duration: 00:32:59
date: 2019-02-10 18:29:08
filename: 7xPI9UQVvOE.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Trump is winning in the swamp
year: '2019'
---

Betsy and Thomas review Trump tweets of February 9 and 10, 2019……but they were so juicy that we had to break the discussion into two parts. Here is the first. We put some of our best memes for the week on screen so make sure to note them. 

The second audio and other remarks and tweets are here: [coming]

Read the manifesto here: https://neoanthroposophy.com/2019/02/08/manifesto-to-save-human-thinking/