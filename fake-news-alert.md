---
audio: https://express.sfo2.digitaloceanspaces.com/aim/HfA0D0EWILk.m4a
audio_duration: 00:28:08
date: 2018-02-12 19:43:33
filename: HfA0D0EWILk.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Fake News Alert
year: '2018'
---

Turns out that the story about the Russian CFO Sergey Panchenko being on the plane that crashed in Russia is a hoax. The plane crash is not the fake news. Please refer to the Daily Caller article below to get the accurate accounting of what occurred. http://dailycaller.com/2018/02/12/harvard-professor-claims-source-died-plane-crash/