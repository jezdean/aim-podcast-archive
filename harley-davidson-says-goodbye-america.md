---
audio: https://express.sfo2.digitaloceanspaces.com/aim/89eWq8PuOQs.m4a
audio_duration: 00:26:03
date: 2018-06-26 19:49:20
filename: 89eWq8PuOQs.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Harley-Davidson Says Goodbye America
year: '2018'
---

Betsy and Thomas decode important Trump Tweets of June 26, 2018. Listen and learn. It is news if you hear it today, but history you need to know tomorrow. To read the tweets: https://truthbits.blog/2018/06/26/trump-tweets-good-bye-to-harley-davidson/