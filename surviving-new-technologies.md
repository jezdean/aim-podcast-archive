---
audio: https://express.sfo2.digitaloceanspaces.com/aim/6TBTQ-6gTVY.m4a
audio_duration: 00:41:54
date: 2018-02-09 20:24:43
filename: 6TBTQ-6gTVY.m4a
playlist: AIM Insights
publisher: AIM
title: Surviving New Technologies
year: '2018'
---

Thomas Paine and John Barnwell continue their conversation about natural ways to counter 5g and EMFs. They discuss C60, aloe vera, sea vegetables, shungite, and other natural methods.

We have a great article on this subject: https://patriots4truth.org/2018/01/31/how-to-antidote-5g/

Here is a recent audio of a related topic: https://youtu.be/ru0rR_HV6Ms