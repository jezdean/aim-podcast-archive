---
audio: https://express.sfo2.digitaloceanspaces.com/aim/SMuYU5euwfM.m4a
audio_duration: 00:34:17
date: 2018-04-05 19:56:12
filename: SMuYU5euwfM.m4a
playlist: AIM Insights
publisher: AIM
title: 'SES: The Vermin Among Us'
year: '2018'
---

In this heartfelt message, Douglas Gabriel and Michael McKibben appeal to the American people to pay attention to what is going on Washington and ask you to demand, in whatever way you feel appropriate, to end the Senior Executive Service and all its related entities and activities. This is the enemy within and we have duty to call it out and destroy the SES before it destroys America.  

SIgn the petition to get rid of SES: https://petitions.whitehouse.gov/petition/remove-unconstitutional-9000-senior-executive-services-ses-members-httpsyoutubeiffnt9jag2g