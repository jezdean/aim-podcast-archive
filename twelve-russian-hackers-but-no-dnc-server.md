---
audio: https://express.sfo2.digitaloceanspaces.com/aim/zJQFjF0-zbw.m4a
audio_duration: 00:32:58
date: 2018-07-14 20:43:19
filename: zJQFjF0-zbw.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Twelve Russian Hackers But No DNC Server
year: '2018'
---

Betsy and Thomas decode Trump’s tweets of July 14, 2018. Is Julian Assange getting ready to be released?  To read the tweets as you listen and see other interesting information: https://truthbits.blog/2018/07/14/twelve-russian-hackers-but-no-dnc-server/