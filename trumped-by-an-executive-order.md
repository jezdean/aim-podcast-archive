---
audio: https://express.sfo2.digitaloceanspaces.com/aim/24QNwfq7u5k.m4a
audio_duration: 00:08:34
date: 2017-12-22 21:14:19
filename: 24QNwfq7u5k.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Trumped by an Executive Order
year: '2017'
---

Betsy and Thomas discuss the executive order that Trump signed on December 21, 2017 entitled: Executive Order Blocking the Property of Persons Involved in Serious Human Rights Abuse or Corruption. See the list on the annex at: https://home.treasury.gov/news/press-releases/sm0243
Support links to this audio are:
https://www.whitehouse.gov/presidential-actions/executive-order-blocking-property-persons-involved-serious-human-rights-abuse-corruption/
And remember what the pre-president Donald Trump told us https://www.youtube.com/watch?v=v9KYenNrtr0&feature=youtu.be