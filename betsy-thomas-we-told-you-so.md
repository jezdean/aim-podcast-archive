---
audio: https://express.sfo2.digitaloceanspaces.com/aim/Betsy-and-Thomas-Right-Again.mp3
audio_duration: 00:49:03
date: 2020-09-30 00:00:00
filename: Betsy-and-Thomas-Right-Again.mp3
publisher: AIM
title: Betsy & Thomas -- We Told You So
year: '2020'
---

Douglas can’t wait to say “I told you so” with the blockbuster reports coming out that Hildabeast was the ‘Russian operative’ she sent the FBI on a witchhunt for. You will laugh along with us because you have known this for years, too!