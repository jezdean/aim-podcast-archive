---
audio: https://express.sfo2.digitaloceanspaces.com/aim/379630002.m4a
audio_duration: 00:46:44
date: 2019-12-15 19:26:01
filename: 379630002.mp3
publisher: AIM
title: 'Rudy Giuliani: Locked and Loaded'
year: '2019'
---

Rudy Giuliani just returned from the Ukraine with a big report to his boss - President Donald Trump. Betsy and Thomas opine as to his findings. We referenced this Cat Report in our discussion: aim4truth.org/2019/12/15/cat-report-239/