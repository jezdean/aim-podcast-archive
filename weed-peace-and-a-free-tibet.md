---
audio: https://express.sfo2.digitaloceanspaces.com/aim/DDKdUjX2CHg.m4a
audio_duration: 00:43:30
date: 2018-12-22 21:43:40
filename: DDKdUjX2CHg.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Weed, PEACE, and a free Tibet
year: '2018'
---

Betsy and Thomas are thrilled with the gifts that Santa Trump is bringing Americans. From the legalization of hemp to troop withdrawal in Korea, Syria and Afghanistan Trump’s sleigh is filled with spectacular gifts of peace and prosperity.