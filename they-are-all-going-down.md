---
audio: https://express.sfo2.digitaloceanspaces.com/aim/Rii1shnQj4M.m4a
audio_duration: 00:36:16
date: 2017-10-28 18:47:22
filename: Rii1shnQj4M.m4a
playlist: Betsy and Thomas
publisher: AIM
title: They are all going down
year: '2017'
---

As the citizens work to bring law and order back to the US from when it was stolen by nefarious means in 1913, everyone will have to face the consequences. Those that have operated outside of the law will need to be indicted and brought to trial. Those who are guilty will need to be punished in a manner outlined by the law.  Citizens, too, will have their consequences for turning a blind eye to the enormous corruption that has become the D.C. swamp.

Anti-Trump dossier here: https://www.documentcloud.org/documents/3259984-Trump-Intelligence-Allegations.html

 A supplemental 500-page truth bomb book is available for free here: https://aim4truthblog.files.wordpress.com/2017/10/cyber-hijack-findings.pdf 

Make sure you are subscribed to TRUTH NEWS HEADLINES to receive free, daily updates. www.aim4truth.org