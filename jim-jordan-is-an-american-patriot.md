---
audio: https://express.sfo2.digitaloceanspaces.com/aim/LLbgfUNU7Ls.m4a
audio_duration: 00:15:25
date: 2018-07-11 00:19:23
filename: LLbgfUNU7Ls.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Jim Jordan is an American Patriot
year: '2018'
---

Betsy and Thomas join Michael McKibben to tell folks about an amazing, courageous American patriot that we all need to be supporting.