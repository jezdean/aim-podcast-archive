---
audio: https://express.sfo2.digitaloceanspaces.com/aim/bSsLm2wDknQ.m4a
audio_duration: 00:51:20
date: 2018-12-03 00:14:40
filename: bSsLm2wDknQ.m4a
playlist: AIM Insights
publisher: AIM
title: Has your mortgage been stolen right under your nose?
year: '2018'
---

Do you have a mortgage? If so, then this is a discussion that may save your home from being stolen from the banks and attorneys who now steal homes from Americans using a craft legal trick. Every homeowner needs to pay attention before you are left homeless like many of the victims Douglas Gabriel has spoken to. Listen to the video and then see our citizens intelligence report on MERS at this link: https://patriots4truth.org/2018/12/03/how-banksters-and-lawyers-are-stealing-homes-from-americans/