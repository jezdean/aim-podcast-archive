---
audio: https://express.sfo2.digitaloceanspaces.com/aim/kbWqa2Lm368.m4a
audio_duration: 00:50:58
date: 2019-02-08 21:53:21
filename: kbWqa2Lm368.m4a
playlist: AIM Insights
publisher: AIM
title: Manifesto to save human thinking
year: '2019'
---

Read the manifesto here: https://neoanthroposophy.com/2019/02/08/manifesto-to-save-human-thinking/