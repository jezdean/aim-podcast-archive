---
audio: https://express.sfo2.digitaloceanspaces.com/aim/-suVm0yxWh0.m4a
audio_duration: 00:44:18
date: 2018-09-16 23:01:46
filename: -suVm0yxWh0.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Massive Government Corruption Revelations Will Trump the Swamp
year: '2018'
---

Betsy and Thomas decode the Trump Tweets of September 15 and 16, 2018. Trump retweets a Devin Nunes video clip which points to the possibility of full transparency on important documents being kept from the American people. Read the tweets and the other information mentioned in the audio: https://truthbits.blog/2018/09/16/massive-government-corruption-revelations-will-trump-the-swamp/