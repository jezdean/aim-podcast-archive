---
audio: https://express.sfo2.digitaloceanspaces.com/aim/DkvVfxZgYd0.m4a
audio_duration: 00:47:57
date: 2018-07-18 13:05:29
filename: DkvVfxZgYd0.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Evil Men Who Rig Our Elections
year: '2018'
---

Learn about election rigging and what citizens need to do to stop it. This link contains the video and some great intel to pass along to others: https://truthbits.blog/2018/07/18/evil-men-who-rig-our-elections/