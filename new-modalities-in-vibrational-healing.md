---
audio: https://express.sfo2.digitaloceanspaces.com/aim/WbwCIJ6FeU0.m4a
audio_duration: 00:59:22
date: 2019-06-16 11:37:07
filename: WbwCIJ6FeU0.m4a
playlist: Betsy and Thomas
publisher: AIM
title: New modalities in vibrational healing
year: '2019'
---

Tyla and Douglas Gabriel review a conference they attended on vibrational science. They review several modalities and devices that they found effective. Of course, you will want to do your own research and draw your own conclusions.