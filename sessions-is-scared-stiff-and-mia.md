---
audio: https://express.sfo2.digitaloceanspaces.com/aim/BCWg78lKk_g.m4a
audio_duration: 00:43:28
date: 2018-08-12 18:59:50
filename: BCWg78lKk_g.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Sessions is scared stiff and MIA
year: '2018'
---

Betsy and Thomas decode Trump’s tweets of August 11 and 12, 2018. Says AG Sessions is scared stiff and MIA and ‘Bikers for Trump’ surround the president’s home in Bedminster as a show of patriot strength and resolve.  To read the tweets as you listen and see other interesting information: https://truthbits.blog/2018/08/12/sessions-is-scared-stiff-and-mia/