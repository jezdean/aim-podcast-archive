---
audio: https://express.sfo2.digitaloceanspaces.com/aim/gbRhGRgtg3E.m4a
audio_duration: 00:45:57
date: 2018-09-20 19:03:32
filename: gbRhGRgtg3E.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Are you lost in space with NASA, satellites and UFOs?
year: '2018'
---

Betsy and Thomas examine why Sessions is resisting POTUS. They also explain why it is that we know so little about our space program. At the end of the discussion Thomas explains what is really happening in Syria. Check out the post here: https://truthbits.blog/2018/09/20/trump-disappointed-with-jeff-sessions/