---
audio: https://express.sfo2.digitaloceanspaces.com/aim/HbU8vC8Fj0g.m4a
audio_duration: 00:34:23
date: 2018-04-02 20:33:59
filename: HbU8vC8Fj0g.m4a
playlist: AIM Insights
publisher: AIM
title: SES Stinks like Sewage
year: '2018'
---

Gabriel and McKibben show you the awful results of a week's worth of mining in the Plum Books. 

Get involved with our movement: https://patriots4truth.org/2018/03/30/jeff-sessions-enemy-of-the-state/

For more information on Jeff Sessions see: https://patriots4truth.org/2018/03/30/jeff-sessions-enemy-of-the-state/

Listen to this eye-opening audio. Then decide how best to educate your audience with this material. Remember, you are welcomed to repost this audio on your own YouTube channel. You can jazz it up and put images, graphs, and visuals that would make it more appealing to your listeners.