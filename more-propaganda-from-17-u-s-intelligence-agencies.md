---
audio: https://express.sfo2.digitaloceanspaces.com/aim/Propaganda-of-17-intelligence-agencies.mp3
audio_duration: 00:57:08
date: 2020-05-03 00:00:00
filename: Propaganda-of-17-intelligence-agencies.mp3
publisher: AIM
title: More Propaganda from 17 U.S. Intelligence Agencies
year: '2020'
---

Betsy and Thomas review the latest press release from the Office of the Director National Intelligence and discuss how Trump’s presidency could go beyond 2024. 

These are the articles referenced in the audio:

https://truthbits.blog/2020/05/03/propaganda-from-17-intelligence-agencies/