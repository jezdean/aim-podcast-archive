---
audio: https://express.sfo2.digitaloceanspaces.com/aim/392816283.m4a
audio_duration: 00:34:58
date: 2020-02-20 16:44:09
filename: 392816283.mp3
publisher: AIM
title: Citizen Grand Juries Empower We the People
year: '2020'
---

Make sure to visit Randall E. White's website and buy a copy of his book to educate yourself about your citizen power. aps-edu.com/
Subscribe to our FREE civic education series at patriots4truth.org/2020/02/20/citizen-grand-juries-empower-we-the-people/