---
audio: https://express.sfo2.digitaloceanspaces.com/aim/IhvH3ntEAJo.m4a
audio_duration: 00:57:52
date: 2018-04-20 21:41:12
filename: IhvH3ntEAJo.m4a
playlist: AIM Insights
publisher: AIM
title: They rule the planet
year: '2018'
---

Douglas Gabriel and Michael McKibben discuss how the United States is ruled by the British Crown through a convolution of secret shares, board control, and SES rat lines that make sure the Brits, through Serco, control the United States government. 

To read more and see the proof yourself, go to: https://americans4innovation.blogspot.com/2018/04/the-shadow-government-uses-ses-serco.html

Then read how the big puzzle fits together: https://patriots4truth.org/2018/04/20/escape-from-prison-planet/