---
audio: https://express.sfo2.digitaloceanspaces.com/aim/mjnkK_Yz_bk.m4a
audio_duration: 00:34:04
date: 2018-02-16 16:25:41
filename: mjnkK_Yz_bk.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Entrepreneurs Screwed
year: '2018'
---

Betsy goes solo and interviews Michael McKibben, Founder and CEO of Leader Technologies about the state of entrepreneurship in America when no one can trust the patent or legal system to protect their inventions.