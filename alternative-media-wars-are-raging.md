---
audio: https://express.sfo2.digitaloceanspaces.com/aim/NsGRRjTZRgU.m4a
audio_duration: 00:17:32
date: 2018-03-26 20:19:34
filename: NsGRRjTZRgU.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Alternative Media Wars Are Raging
year: '2018'
---

The war continues and the challenge to other alternative media channels has not been accepted. Will they expose the Senior Executive Services and the Global Engagement Center connections that they have?