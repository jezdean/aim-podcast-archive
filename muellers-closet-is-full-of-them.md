---
audio: https://express.sfo2.digitaloceanspaces.com/aim/4GthwvhP-p8.m4a
audio_duration: 00:01:52
date: 2017-12-09 19:02:10
filename: 4GthwvhP-p8.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Muellers Closet is Full of Them
year: '2017'
---

Betsy, Thomas, and the Anonymous Patriots set out to see if there were any more skeletons in Robert Mueller’s closet and what they found even blew them away. 

Here are the articles that they reference in their discussion:

Mueller and Patrick: https://aim4truth.org/2017/12/09/muellers-pathetic-attempt-to-overthrow-trump-and-prepare-his-2020-successor-fails-miserably/

Chelsea Clinton: https://aim4truth.org/2017/12/07/the-incredible-backstory-of-chelsea-clinton/

911: https://aim4truth.org/2017/02/18/treason-who-terrorized-americans-and-the-world-on-9-11/

Paradox of Progress: https://www.dni.gov/files/documents/nic/GT-Full-Report.pdf