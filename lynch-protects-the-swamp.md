---
audio: https://express.sfo2.digitaloceanspaces.com/aim/Esi6fJ-G4qk.m4a
audio_duration: 00:21:14
date: 2018-04-29 23:06:57
filename: Esi6fJ-G4qk.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Lynch Protects the Swamp
year: '2018'
---

Betsy and Thomas discuss some breaking news about Loretta Lynch’s involvement in the coup. The article referenced is from Conservative Treehouse and can be found here: 

https://theconservativetreehouse.com/2018/04/29/an-updated-review-of-details-within-lisa-page-and-peter-strzok-text-messaging/