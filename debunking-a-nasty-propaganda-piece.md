---
audio: https://express.sfo2.digitaloceanspaces.com/aim/arvinder-sambei-and-the-coup.mp3
audio_duration: 00:42:35
date: 2020-08-09 00:00:00
filename: arvinder-sambei-and-the-coup.mp3
publisher: AIM
title: Debunking a Nasty Propaganda Piece
year: '2020'
---

Betsy and Thomas comment on this propaganda piece circulating the internet right now: https://taibbi.substack.com/p/the-spies-who-hijacked-america

https://truthbits.blog/2020/08/09/steven-p-schrage-propaganda-to-protect-the-queen/