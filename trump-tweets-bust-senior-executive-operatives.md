---
audio: https://express.sfo2.digitaloceanspaces.com/aim/VhR-ImgqzG4.m4a
audio_duration: 00:40:10
date: 2018-06-15 19:17:55
filename: VhR-ImgqzG4.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Trump Tweets Bust Senior Executive Operatives
year: '2018'
---

Betsy and Thomas review Trump Tweets of June 15. Link here for audio and tweets in one URL: https://truthbits.blog/2018/06/15/trump-tweets-bust-senior-executive-service-operatives/