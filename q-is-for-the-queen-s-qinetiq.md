---
audio: https://express.sfo2.digitaloceanspaces.com/aim/392798558.m4a
audio_duration: 01:03:34
date: 2020-02-20 15:20:47
filename: 392798558.mp3
publisher: AIM
title: Q is for the Queen's Qinetiq
year: '2020'
---

Big, breaking intelligence inside this video. We will be linking our research, which is extensive and will contain all of the source docs: [coming]   
Make sure you download this video and upload to your own video channel. This intelligence drop will be useful to researchers around the world.