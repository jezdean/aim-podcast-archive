---
audio: https://express.sfo2.digitaloceanspaces.com/aim/wR3Mjyx_rcY.m4a
audio_duration: 00:24:23
date: 2017-11-15 01:20:34
filename: wR3Mjyx_rcY.m4a
playlist: Leader Tech
publisher: AIM
title: The Big Lie
year: '2017'
---

The full interview series is here: https://aim4truth.org/2017/11/21/facebook-unmasked-how-the-worlds-most-relevant-entrepreneur-was-screwed-by-zuckerberg/

Listen to Michael McKibben tell our Thomas Paine what it took him to invent the technology that made social media the amazing tool it is today. Then find another channel and listen to Mark Zuckerberg's explanation of how he did it. Use some critical thought and decide for yourself who was the real inventor.