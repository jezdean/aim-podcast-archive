---
audio: https://express.sfo2.digitaloceanspaces.com/aim/Via5-N0vsR0.m4a
audio_duration: 00:42:18
date: 2018-11-05 18:17:22
filename: Via5-N0vsR0.m4a
playlist: AIM Insights
publisher: AIM
title: Calling on 'Network Engineer Patriots' to Election Cyber Stations
year: '2018'
---

Listen to Michael McKibben and Douglas Gabriel explain how white hat cyber warriors can protect election integrity. Surprise bonus at the end when Michael makes a huge discovery in Florida elections! To continue to educate yourself, check out the great stuff we have for you in this link: https://aim4truth.org/2018/11/05/emergency-call-to-all-white-hat-patriot-network-engineers-to-stop-third-parties-from-rigging-the-vote-tallies-in-your-state/