---
audio: https://express.sfo2.digitaloceanspaces.com/aim/4HBx1VxJ2Vw.m4a
audio_duration: 00:50:19
date: 2018-06-28 19:50:35
filename: 4HBx1VxJ2Vw.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Dragon Energy Too Hot to Handle
year: '2018'
---

Betsy and Thomas decode important Trump Tweets of June 27, 2018. Listen and learn. It is news if you hear it today, but history you need to know tomorrow. To read the tweets: https://truthbits.blog/2018/06/28/dragon-energy-too-hot-to-handle/