---
audio: https://express.sfo2.digitaloceanspaces.com/aim/WNupSkgn6Kc.m4a
audio_duration: 00:25:15
date: 2018-04-21 17:31:38
filename: WNupSkgn6Kc.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Sins of omission
year: '2018'
---

Have you been following the unraveling of the swamp? Here are some articles that support claims that Thomas makes in this audio:

Brennan’s secret trip to Russia: http://www.thegatewaypundit.com/2018/04/developing-obama-cia-chief-john-brennan-made-secret-visit-to-russia-around-same-time-as-fusion-gps-produced-anti-trump-memos/

Comey Crimes: https://aim4truth.org/2017/03/29/6438/

https://aim4truth.org/2018/02/18/we-the-people-make-official-charges-of-28-counts-of-treason/

https://aim4truth.org/2017/09/06/james-comey-exposed-chief-election-meddler-and-instigator-for-the-overthrow-trump/

https://aim4truth.org/2017/12/17/doj-inspector-general-michael-horowitz-was-handpicked-by-soros-co/

https://patriots4truth.org/2018/03/30/jeff-sessions-enemy-of-the-state/