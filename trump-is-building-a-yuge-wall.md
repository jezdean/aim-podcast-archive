---
audio: https://express.sfo2.digitaloceanspaces.com/aim/N6oaNlfnRXQ.m4a
audio_duration: 00:35:05
date: 2019-02-10 21:05:10
filename: N6oaNlfnRXQ.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Trump is building a YUGE wall!
year: '2019'
---

Betsy and Thomas review the second part of Trump tweets of February 9 and 10, 2019……but they were so juicy that we had to break the discussion into two parts. Here is the first one: https://youtu.be/7xPI9UQVvOE  We put some of our best memes for the week on screen so make sure to note them. 

Other remarks and tweets are here: https://truthbits.blog/2019/02/10/trump-is-winning-in-the-swamp/

Read the manifesto here: https://neoanthroposophy.com/2019/02/08/manifesto-to-save-human-thinking/