---
audio: https://express.sfo2.digitaloceanspaces.com/aim/M1fOmuBewhk.m4a
audio_duration: 00:17:43
date: 2018-10-01 21:10:00
filename: M1fOmuBewhk.m4a
playlist: AIM Insights
publisher: AIM
title: Berners-Lee and the Fake New Web
year: '2018'
---

Douglas Gabriel and Michael McKibben discuss the latest news from Sir Timothy Berners-Lee who olans to upend the World Wide Web. See the article here: https://www.fastcompany.com/90243936/exclusive-tim-berners-lee-tells-us-his-radical-new-plan-to-upend-the-world-wide-web