---
audio: https://express.sfo2.digitaloceanspaces.com/aim/AT48ze_Ixp0.m4a
audio_duration: 00:02:05
date: 2018-12-19 16:17:07
filename: AT48ze_Ixp0.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Christmas present for AIM cats
year: '2018'
---

To receive you special gift of 5 books written by Tyla and Douglas Gabriel, click here: https://conta.cc/2LqEG4h Then help us spread the love and joy to your friends and family.