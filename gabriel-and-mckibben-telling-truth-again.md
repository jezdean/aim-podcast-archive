---
audio: https://express.sfo2.digitaloceanspaces.com/aim/lOjSqtxagWQ.m4a
audio_duration: 00:27:09
date: 2019-02-26 01:51:25
filename: lOjSqtxagWQ.m4a
playlist: AIM Insights
publisher: AIM
title: Gabriel and McKibben telling truth again
year: '2019'
---

Douglas, Michael, and Tyla are settling in for a few days of audios for our amazing truth community that is winning the information war on all fronts. The night before we start recording, we like to sit around and warm up the conversation. This time, we thought you might like to join us. We know it can be lonely out there to find thinkers like you and us.