---
audio: https://express.sfo2.digitaloceanspaces.com/aim/-zWWJ1BzKgI.m4a
audio_duration: 00:53:36
date: 2018-09-13 19:23:49
filename: -zWWJ1BzKgI.m4a
playlist: AIM Insights
publisher: AIM
title: Trump's September Surprise is a Doozy
year: '2018'
---

Douglas Gabriel and Michael McKibben drill down on Trump’s Executive Order on foreign and domestic interference in the elections. BOOM! September surprise. All the EOs discussed can be found in this link: 
https://patriots4truth.org/2018/09/13/executive-order-is-huge-september-surprise/