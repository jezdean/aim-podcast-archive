---
audio: https://express.sfo2.digitaloceanspaces.com/aim/YP1cuoxaYac.m4a
audio_duration: 00:33:06
date: 2018-07-11 16:01:34
filename: YP1cuoxaYac.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Will this be the end of NATO?
year: '2018'
---

Betsy and Thomas explain why Trump’s tweets of July 10 and 11, 2018 and his bilateral breakfast meeting to kick off NATO talks. To read the tweets as you listen: https://truthbits.blog/2018/07/11/will-this-be-the-end-of-nato/