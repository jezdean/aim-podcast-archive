---
audio: https://express.sfo2.digitaloceanspaces.com/aim/Au5idOpjU34.m4a
audio_duration: 00:41:03
date: 2018-08-30 22:35:19
filename: Au5idOpjU34.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Trump puts them in the DOGHOUSE
year: '2018'
---

Michael McKibben of Leader Technologies joins Betsy and Thomas for Trump tweet decodes of August 30, 2018. To read the tweets as you listen and see some other great info: https://truthbits.blog/2018/08/30/trump-puts-them-in-the-doghouse/