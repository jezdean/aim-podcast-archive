---
audio: https://express.sfo2.digitaloceanspaces.com/aim/bsHxWIrcOvk.m4a
audio_duration: 00:53:57
date: 2018-07-24 18:10:48
filename: bsHxWIrcOvk.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Coach ‘T’ Tweets that Team America is Doing Great
year: '2018'
---

Betsy and Thomas decode Trump’s tweets of July 24, 2018. POTUS tweets about Lockheed Martin and Thomas excoriates the company for being a foreign held entity that does nothing but scam American taxpayers. To read the tweets as you listen and see other interesting information: https://truthbits.blog/2018/07/24/coach-t-tweets-that-team-america-is-doing-great/