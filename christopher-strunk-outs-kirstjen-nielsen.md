---
audio: https://express.sfo2.digitaloceanspaces.com/aim/KqfH12Vu2RM.m4a
audio_duration: 00:45:21
date: 2018-11-13 10:37:04
filename: KqfH12Vu2RM.m4a
playlist: Swamp with Strunk
publisher: AIM
title: Christopher Strunk outs Kirstjen Nielsen
year: '2018'
---

Christopher Strunk updates the community with his last visit to the Department of Homeland Security where a Secret Service agent asked him unusual questions. He explains his next steps in salvaging the rigged election system and finding out why DHS would consider him a concern to Secretary Nielsen.