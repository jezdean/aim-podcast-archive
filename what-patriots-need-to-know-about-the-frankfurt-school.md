---
audio: https://express.sfo2.digitaloceanspaces.com/aim/FvRHchv6Dlk.m4a
audio_duration: 00:41:20
date: 2019-10-31 00:06:48
filename: FvRHchv6Dlk.m4a
playlist: AIM Insights
publisher: AIM
title: What Patriots need to know about the Frankfurt School
year: '2019'
---

Learn more about the Frankfurt School: https://truthbits.blog/2019/10/31/what-is-the-frankfurt-school/