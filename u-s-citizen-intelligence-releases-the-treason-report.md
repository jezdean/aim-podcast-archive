---
audio: https://express.sfo2.digitaloceanspaces.com/aim/Z4zJhatRPwU.m4a
audio_duration: 00:29:48
date: 2019-05-13 19:57:11
filename: Z4zJhatRPwU.m4a
playlist: AIM Insights
publisher: AIM
title: U S  Citizen Intelligence Releases the Treason Report
year: '2019'
---

Douglas Gabriel and Michael McKibben discuss the Treason Report, a U.S. citizen intelligence report on high crimes, treason, and espionage in D.C.. 

Read the report here: https://aim4truth.org/2019/05/13/citizens-release-treason-report/