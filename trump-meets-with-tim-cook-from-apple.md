---
audio: https://express.sfo2.digitaloceanspaces.com/aim/zuORbwO6fcY.m4a
audio_duration: 00:30:48
date: 2018-08-11 21:08:37
filename: zuORbwO6fcY.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Trump meets with Tim Cook from Apple
year: '2018'
---

Betsy and Thomas decode Trump’s tweets of August 10, 2018 where Trump gives a shout out to Nancy Pelosi and has Tim Cook of Apple over for dinner.  To read the tweets as you listen and see other interesting information: https://truthbits.blog/2018/08/11/was-tim-cook-apple-pie-a-la-mode/