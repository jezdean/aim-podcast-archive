---
audio: https://express.sfo2.digitaloceanspaces.com/aim/The-Big-Lie-of-Social-Media.mp3
audio_duration: 01:10:13
date: 2020-05-29 00:00:00
filename: The-Big-Lie-of-Social-Media.mp3
publisher: AIM
title: The Big Lie of Social Media
year: '2020'
---

Douglas Gabriel and Michael McKibben look at the fine print inside the Executive Order.