---
audio: https://express.sfo2.digitaloceanspaces.com/aim/LMoHf3VOv4g.m4a
audio_duration: 00:53:50
date: 2019-05-26 20:11:37
filename: LMoHf3VOv4g.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Cannabis and the Secret of Enlightenment
year: '2019'
---

To read more about the endocannabinoid system, see this article by Douglas Gabriel: https://patriots4truth.org/2019/05/21/endocannabinoids/