---
audio: https://express.sfo2.digitaloceanspaces.com/aim/FDU34lCmPl8.m4a
audio_duration: 00:16:36
date: 2019-07-30 19:43:56
filename: FDU34lCmPl8.m4a
playlist: AIM Insights
publisher: AIM
title: Highlands Group vs Trump the Scot
year: '2019'
---

In this audio Michael McKibben and Douglas Gabriel vet an article written by a Brit reporter - Nafeez Ahmed and posted at Medium. You will learn how the article is a sophisticated form of propaganda. Hopefully, our "lesson" will help you be able to discern future articles like this that omit information and lead to false conclusions. 

Read the articles here: https://patriots4truth.org/2019/07/30/highlands-group-vs-trump-the-scot/