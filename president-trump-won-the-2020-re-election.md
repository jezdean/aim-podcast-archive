---
audio: https://express.sfo2.digitaloceanspaces.com/aim/trump-won-the-2020-election.mp3
audio_duration: 00:49:37
date: 2020-11-04 00:00:00
filename: trump-won-the-2020-election.mp3
publisher: AIM
title: President Trump Won the 2020 Re-Election
year: '2020'
---

Betsy and Thomas explain that we won the 2020 election, but will have some more obstacles through the end of December….just like Douglas predicted earlier in the year. Michael also joins us in the analysis of the information war.

https://truthbits.blog/2020/11/04/president-trump-won-the-2020-re-election/