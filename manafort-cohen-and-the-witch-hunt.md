---
audio: https://express.sfo2.digitaloceanspaces.com/aim/jE68KSDwMgU.m4a
audio_duration: 00:45:10
date: 2018-08-22 23:23:46
filename: jE68KSDwMgU.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Manafort, Cohen, and the Witch Hunt
year: '2018'
---

Yes, we know the picture might cause nausea...so don't look at it. Just listen to us and create your own pictures in your mind's eye.
Betsy and Thomas decode Trump’s tweets of August 22, 2018.  To read the tweets and see what other surprises we have for you open: https://truthbits.blog/2018/08/22/manafort-cohen-and-the-witch-hunt/