---
audio: https://express.sfo2.digitaloceanspaces.com/aim/AzZ6QwCMwLA.m4a
audio_duration: 00:44:49
date: 2017-12-24 21:13:06
filename: AzZ6QwCMwLA.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Defeating the New World Order
year: '2017'
---

This is for advanced students of the American Intelligence Media. Betsy and Thomas describe the pyramidal structure of global governance and how it is being dismantled as we speak.

To download a PDF of the chart that is referred to: https://aim4truthblog.files.wordpress.com/2017/12/deep-state-chart.pdf