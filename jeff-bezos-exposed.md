---
audio: https://express.sfo2.digitaloceanspaces.com/aim/jzwMxbkF_0k.m4a
audio_duration: 00:22:03
date: 2017-12-01 19:43:42
filename: jzwMxbkF_0k.m4a
playlist: Leader Tech
publisher: AIM
title: Jeff Bezos Exposed
year: '2017'
---

Douglas Gabriel of www.aim4truth.org Michael McKibben of Leader Technologies https://aim4truth.org/2017/11/21/facebook-unmasked-how-the-worlds-most-relevant-entrepreneur-was-screwed-by-zuckerberg/ discuss who Jeff Bezos and Amazon really are and why we should be asking more questions about his and his company’s questionable background.

The guys also discuss a company named Global Thermostat, Accel Partners, and Banker’s Trust. Also discussed are names that may not be familiar with listeners now, but will make more prominence in the news when the media finally catches up with us: Graciela Chichilnisky, Stephen Chu, and Edgar Miles Bronfman. 

Are you receiving your FREE Truth News Headlines, delivered daily to your inbox? Subscribe at www.aim4truth.org