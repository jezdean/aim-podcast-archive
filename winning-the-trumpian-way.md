---
audio: https://express.sfo2.digitaloceanspaces.com/aim/387802776.m4a
audio_duration: 00:29:19
date: 2020-01-28 16:10:19
filename: 387802776.mp3
publisher: AIM
title: Winning the Trumpian Way
year: '2020'
---

Betsy and Thomas review the President's defense in the Senate Impeachment hearing on January 27 and 28, 2020.