---
audio: https://express.sfo2.digitaloceanspaces.com/aim/xMQbzAUCCiY.m4a
audio_duration: 00:34:13
date: 2017-10-05 19:14:04
filename: xMQbzAUCCiY.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Will Mandalay False Flag Lead to Gun Grab
year: '2017'
---

Thomas and Betsy review the Las Vegas shooting and suggest that listeners refer to two recent articles they posted on www.aim4truth.org. 

https://aim4truth.org/2017/10/03/breaking-false-flag-in-las-vegas/

https://aim4truth.org/2017/10/04/analyzing-the-mandalay-massacre-false-flag/