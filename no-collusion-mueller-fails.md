---
audio: https://express.sfo2.digitaloceanspaces.com/aim/KqmV5xrGuKQ.m4a
audio_duration: 00:43:52
date: 2018-09-08 18:57:23
filename: KqmV5xrGuKQ.m4a
playlist: Betsy and Thomas
publisher: AIM
title: No Collusion. Mueller Fails.
year: '2018'
---

As Betsy and Thomas decode the Trump Tweets of September 8, 2018. Trump is not indicted for collusion and Mueller presents a nothingburger.  To read the tweets and the other information mentioned in the audio: https://truthbits.blog/2018/09/08/no-collusion-mueller-fails/