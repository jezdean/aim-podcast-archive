---
audio: https://express.sfo2.digitaloceanspaces.com/aim/I_LlrVFkxRY.m4a
audio_duration: 00:23:15
date: 2018-01-10 22:25:59
filename: I_LlrVFkxRY.m4a
playlist: Betsy and Thomas
publisher: AIM
title: The Wall is Under Construction
year: '2018'
---

The wall is already under construction and DACA will terminate in March unless Congress can come up with an immigration plan by then - which is most likely a non-event. So, DACA folks, start getting those bags packed. Obama isn't your protector anymore.