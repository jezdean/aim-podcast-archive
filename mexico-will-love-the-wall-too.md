---
audio: https://express.sfo2.digitaloceanspaces.com/aim/FLDRJHb6mIk.m4a
audio_duration: 00:25:04
date: 2018-06-23 00:01:07
filename: FLDRJHb6mIk.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Mexico will love the wall, too!
year: '2018'
---

Betsy and Thomas decode important Trump Tweets of June 22, 2018. Listen and learn. It is news if you hear it today, but history you need to know tomorrow. To see the support material that was referenced: https://truthbits.blog/2018/06/22/mexico-will-love-the-wall-too/