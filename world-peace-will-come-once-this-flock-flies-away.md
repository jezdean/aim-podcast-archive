---
audio: https://express.sfo2.digitaloceanspaces.com/aim/RcAiCzQiwxI.m4a
audio_duration: 00:36:17
date: 2018-08-17 15:46:10
filename: RcAiCzQiwxI.m4a
playlist: Betsy and Thomas
publisher: AIM
title: World Peace Will Come...once this flock flies away
year: '2018'
---

Betsy and Thomas decode Trump’s tweets of August 16 and 17, 2018. The thumbnail says it all. To read the tweets as you listen and see other interesting information: https://truthbits.blog/2018/08/17/world-peace-will-come/