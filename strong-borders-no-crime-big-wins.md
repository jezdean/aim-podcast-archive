---
audio: https://express.sfo2.digitaloceanspaces.com/aim/JTyUKq9gJgc.m4a
audio_duration: 00:38:08
date: 2018-06-27 19:45:26
filename: JTyUKq9gJgc.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Strong Borders - No Crime - Big Wins!
year: '2018'
---

Betsy and Thomas decode important Trump Tweets of June 26 27, 2018. Listen and learn. It is news if you hear it today, but history you need to know tomorrow. To read the tweets: https://truthbits.blog/2018/06/27/twitter-censors-potus/