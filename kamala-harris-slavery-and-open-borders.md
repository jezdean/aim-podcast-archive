---
audio: https://express.sfo2.digitaloceanspaces.com/aim/3ygpSHAW6ao.m4a
audio_duration: 00:46:16
date: 2019-07-18 18:46:59
filename: 3ygpSHAW6ao.m4a
playlist: AIM Insights
publisher: AIM
title: Kamala Harris, slavery, and open borders
year: '2019'
---

See the evidence here: https://aim4truth.org/2019/07/17/kamala-harris-and-the-white-irish-slave-trade/