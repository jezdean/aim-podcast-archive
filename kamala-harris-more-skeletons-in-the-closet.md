---
audio: https://express.sfo2.digitaloceanspaces.com/aim/EKX7JezSk4U.m4a
audio_duration: 00:48:42
date: 2019-07-17 00:21:07
filename: EKX7JezSk4U.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Kamala Harris - More Skeletons in the Closet
year: '2019'
---

Betsy and Thomas tackle four issues today - from Iran to Peter Thiel's dust up with Google, to the four women of the Democrat apocalypse. But what is really a surprise is what we are finding in Kamala's family closet of secrets and sins.