---
audio: https://express.sfo2.digitaloceanspaces.com/aim/iUJUKIPab0A.m4a
audio_duration: 00:43:53
date: 2017-11-20 18:12:45
filename: iUJUKIPab0A.m4a
playlist: Leader Tech
publisher: AIM
title: 'Trillion Dollar Rip-Off: Social Networking is a Stolen Trade Secret'
year: '2017'
---

Here is the full interview series: https://aim4truth.org/2017/11/21/facebook-unmasked-how-the-worlds-most-relevant-entrepreneur-was-screwed-by-zuckerberg/

Douglas Gabriel interviews Michael McKibben in PART 1 of SIX interviews. One of the largest government sponsored industrial espionage thefts of copyrights, trade secrets, and patents in modern times was the theft of scalable social networking inventions. The technology and programming code that underlie Facebook, Gmail, YouTube, Twitter, Instagram and most the other large-scale social networking companies runs on Leader Technologies’ intellectual property.

It was stolen by a group of criminal lawyers, judges, spies and bankers working with complete impunity and in total disregard for the law. Under the guise of the IBM Eclipse Foundation, James P. Chandler III (who was a national security advisor and top White House attorney) led the group of criminals who, interestingly enough, are also appearing in the news currently due to their most recently discovered crimes, along with John Podesta, Robert Mueller, Rod Rosenstein, John Breyer, James Breyer, Larry Summers, Yuri Milner, Alisher Usmanov, Mark Zuckerberg, Sheryl Sandberg, Bill and Hillary Clinton, and a host of others who are not so well known.