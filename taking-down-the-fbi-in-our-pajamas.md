---
audio: https://express.sfo2.digitaloceanspaces.com/aim/raTVDh2N2qM.m4a
audio_duration: 00:44:55
date: 2018-10-20 21:23:07
filename: raTVDh2N2qM.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Taking down the FBI in our pajamas
year: '2018'
---

Betsy and Thomas look at the truth of Osama bin Laden’s death (and it isn’t what is in Michael D Moore’s book about how he dismantled the FBI in his pjs) and the demise of Senior Executive Service operative Rod the Rat Rodentstein. Continue your citizen education here: https://truthbits.blog/2018/10/20/taking-down-the-fbi-in-our-pajamas/