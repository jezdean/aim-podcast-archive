---
audio: https://express.sfo2.digitaloceanspaces.com/aim/ugI4sOmE6vg.m4a
audio_duration: 00:34:32
date: 2018-06-26 02:55:36
filename: ugI4sOmE6vg.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Peter Strzok and Hating Frauds of the FBI and DOJ
year: '2018'
---

Betsy and Thomas decode important Trump Tweets of June 24-25, 2018. Listen and learn. It is news if you hear it today, but history you need to know tomorrow. To read the tweets: https://truthbits.blog/2018/06/26/peter-strzok-and-hating-frauds-at-the-fbi-and-doj/