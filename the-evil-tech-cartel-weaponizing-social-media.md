---
audio: https://express.sfo2.digitaloceanspaces.com/aim/MjSxEkez31o.m4a
audio_duration: 00:36:36
date: 2017-11-21 00:27:50
filename: MjSxEkez31o.m4a
playlist: Leader Tech
publisher: AIM
title: 'The Evil Tech-Cartel: Weaponizing Social Media'
year: '2017'
---

Here is the full interview series: https://aim4truth.org/2017/11/21/facebook-unmasked-how-the-worlds-most-relevant-entrepreneur-was-screwed-by-zuckerberg/

Patent theft is accomplished through the Patent Trial and Appeals Board and the many laws that allow the US Patent and Trademark Office to confiscate and control any patent deemed a national security issue. Once confiscated, the inventor is not compensated for the out-right theft of the patent. Stiff penalties, including incarceration, accompany these seemingly “illegal” patent laws that can steal any patent that the military (Department of Defense) or cyber-intelligence (all 17 agencies) considers a patent that could be turned into a weapon.

Once stolen, the government uses a complex group of public, and secret, defense agencies, corporate contractors, and crooked bankers to fund the weaponization of trade secrets, patents, copyrights and other forms of intellectual property. Usually, the government builds in a “back-door” to the technology--so that they can control and manipulate the weaponized property. Then, they fund a “front-man” as a stooge to run a private corporation that is built on the weaponized invention.