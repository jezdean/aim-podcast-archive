---
audio: https://express.sfo2.digitaloceanspaces.com/aim/389823991.m4a
audio_duration: 00:40:09
date: 2020-02-06 15:44:41
filename: 389823991.mp3
publisher: AIM
title: Citizens Take Legal Action Against the Swamp
year: '2020'
---

We placed the written materials and video in an easy to share link here: truthbits.blog/2020/02/06/citizens-take-legal-action-against-the-swamp/