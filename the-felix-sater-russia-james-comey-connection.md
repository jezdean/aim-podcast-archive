---
audio: https://express.sfo2.digitaloceanspaces.com/aim/1L2JHsyzjIw.m4a
audio_duration: 00:33:05
date: 2017-09-02 18:09:41
filename: 1L2JHsyzjIw.m4a
playlist: Betsy and Thomas
publisher: AIM
title: The Felix Sater -Russia - James Comey Connection
year: '2017'
---

Thomas explains how a Trump associate named Felix Sater is being used by James Comey as another failed attempt of trying to show a nefarious Trump – Russia connection. Thomas also reviews the updates on the connections of Robert Mueller, Carter Page, Paul Manafort, Michael Flynn, and Michael Cohen. Thomas points to an article from New York magazine on Felix Sater that provides much detail: http://nymag.com/daily/intelligencer/2017/08/felix-sater-donald-trump-russia-investigation.html

Truth News Headlines are posted daily at www.aim4truth.org