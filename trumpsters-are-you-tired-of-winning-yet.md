---
audio: https://express.sfo2.digitaloceanspaces.com/aim/inCDkis2ZhU.m4a
audio_duration: 00:32:38
date: 2018-10-02 23:35:19
filename: inCDkis2ZhU.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Trumpsters, are you tired of winning yet?
year: '2018'
---

Betsy and Thomas review Trump Tweets of October 1 and 2, 2018 which point to the new deal made between the US, Mexico, and Canada. They also discuss ongoing developments in the confirmation hearings of Kavanaugh.  To read the tweets and supplemental items: https://truthbits.blog/2018/10/02/mexico-and-canada-get-on-board-the-trump-train/