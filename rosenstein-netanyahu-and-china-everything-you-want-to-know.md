---
audio: https://express.sfo2.digitaloceanspaces.com/aim/nYEdz5v-juA.m4a
audio_duration: 00:37:58
date: 2018-09-26 23:17:17
filename: nYEdz5v-juA.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Rosenstein, Netanyahu, and China -Everything you want to know
year: '2018'
---

Betsy and Thomas review tweets of September 25 and 26, 2018. Today Thomas gives special attention to Zionism, the Greater Israel project, and Netanyahu. Read the tweets and check for other surprises: https://truthbits.blog/2018/09/26/rosenstein-netanyahu-and-china-everything-you-want-to-know/