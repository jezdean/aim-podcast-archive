---
audio: https://express.sfo2.digitaloceanspaces.com/aim/AB0CVQzBXHU.m4a
audio_duration: 00:48:25
date: 2019-06-21 20:03:44
filename: AB0CVQzBXHU.m4a
playlist: AIM Insights
publisher: AIM
title: Trumping socialist media with special key code
year: '2019'
---

Michael McKibben of Leader Technologies and Douglas Gabriel of AIM hand President Trump the key code to take over all socialist media. Click here to read the steps proposed and the logistics. Then be a patriot 'information warrior’ and educate your circle of influence: https://truthbits.blog/2019/06/21/trumping-socialist-media/