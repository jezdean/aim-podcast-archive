---
audio: https://express.sfo2.digitaloceanspaces.com/aim/LQmeFVhWM18.m4a
audio_duration: 00:44:35
date: 2018-03-11 20:15:23
filename: LQmeFVhWM18.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Betsy and Thomas talk about puppies, China, and Ormus
year: '2018'
---

Betsy and Thomas love this little puppy image and invite listeners to step in and hear the real truth about China, that you will never hear on main stream media. Get on in here! And don't leave without giving the puppy a bone (i.e. thumbs up)

Link to Ormus: https://patriots4truth.org/2018/03/10/lets-make-ormus/

Thomas recommends this article about China: https://qz.com/1223483/broadcom-a-company-trump-called-really-great-faces-a-national-security-investigation/