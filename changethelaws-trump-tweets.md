---
audio: https://express.sfo2.digitaloceanspaces.com/aim/UmQt0Dmq-3Q.m4a
audio_duration: 00:39:02
date: 2018-06-19 18:01:43
filename: UmQt0Dmq-3Q.m4a
playlist: Betsy and Thomas
publisher: AIM
title: '#CHANGETHELAWS Trump Tweets'
year: '2018'
---

Betsy and Thomas look at the Trump’s #CHANGETHELAWS tweets. To read the tweets as you listen: https://truthbits.blog/2018/06/19/trump-tweets-changethelaws/