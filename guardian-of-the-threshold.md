---
audio: https://express.sfo2.digitaloceanspaces.com/aim/dweller-on-the-threshold.mp3
audio_duration: 00:49:51
date: 2020-08-01 00:00:00
filename: dweller-on-the-threshold.mp3
publisher: AIM
title: Guardian of the Threshold
year: '2020'
---

Tyla and Douglas Gabriel discuss the Dweller on the Threshold.