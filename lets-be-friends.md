---
audio: https://express.sfo2.digitaloceanspaces.com/aim/tVV7sHYhi9I.m4a
audio_duration: 00:37:41
date: 2018-03-28 01:25:50
filename: tVV7sHYhi9I.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Lets be friends
year: '2018'
---

Betsy and Thomas love puppies and think more could be done to share puppy love all over the world. If you are a smithie or jeweler feel free to use this video - maybe with a different thumbnail. Whatever your audience likes. 

Let's share puppy love and licks all over cyberspace.