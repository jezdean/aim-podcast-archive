---
audio: https://express.sfo2.digitaloceanspaces.com/aim/t9RweiDvoLk.m4a
audio_duration: 00:18:22
date: 2019-08-12 19:06:36
filename: t9RweiDvoLk.m4a
playlist: History of Banking
publisher: AIM
title: Douglas Gabriel - Warlord Bankers and the Federal Reserve
year: '2019'
---

We are pleased to feature this video by AIM Patriot Infotoons who took one of Douglas’ lectures on Venetian banking and created an animation. Make sure to subscribe to Infotoons here: https://www.youtube.com/channel/UCpVUXHa1JfNnnc4u3q7j-5Q

Click here to see the entire lesson: https://patriots4truth.org/2019/08/12/warlord-bankers-and-the-federal-reserve/