---
audio: https://express.sfo2.digitaloceanspaces.com/aim/dXT5_Vp8qP4.m4a
audio_duration: 01:09:58
date: 2017-09-08 01:02:25
filename: dXT5_Vp8qP4.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Betsy and Thomas Share Some Hope
year: '2017'
---

THIS VIDEO IS NOT ONE HOUR--More like 40 minutes. Betsy had a glitch in the upload, but has moved on to the next project. 

In this show, Betsy and Thomas answer two questions from recent videos. The first is about with Assad, Syria, ISIS, Al Qaeda. ‘Nature Girl’ and other viewers wanted Thomas to explain why he said in a recent video “Assad created ISIS and Al Qaeda.” Some viewers were under the impression that these were CIA-created. Thomas spends the first half of the show discussing the nuances of these different factions. 

23:00 mark: A viewer Dennis Velintino asked a question about how WE THE PEOPLE can put an end to the corruption in D.C. and restore the constitutional republic. Betsy and Thomas spend the rest of the show on this topic.

To read the CITIZENS INTELLIGENCE REPORTS (CIR) that the American Intelligence Media has researched for you, please go to www.aim4truth.org. Make sure to sign up for our daily posts and peruse our amazing CIRs that you can use to educate your elected officials.