---
audio: https://express.sfo2.digitaloceanspaces.com/aim/pCK_69Yik_c.m4a
audio_duration: 00:33:19
date: 2019-02-17 20:27:38
filename: pCK_69Yik_c.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Citizens Demand Comey's Arrest for Treason!!
year: '2019'
---

On February 18, 2018, we posted an article “We the People make Official Charges of 28 Counts of Treason,” https://aim4truth.org/2018/02/18/we-the-people-make-official-charges-of-28-counts-of-treason/. At our one year anniversary, we are revisiting this article and adding audio descriptions to each count to bring this story to current events.