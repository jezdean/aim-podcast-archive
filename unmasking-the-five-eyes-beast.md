---
audio: https://express.sfo2.digitaloceanspaces.com/aim/ACQ4PehICIg.m4a
audio_duration: 00:50:16
date: 2019-05-24 21:36:42
filename: ACQ4PehICIg.m4a
playlist: AIM Insights
publisher: AIM
title: Unmasking the Five Eyes Beast
year: '2019'
---

Douglas Gabriel and Michael McKibben reveal the origins of this thing called “Five Eyes”. It blew us away when we saw who created this spying network and guess what? The British enemy uses it to spy on the United States. Listen and learn who the enemy is.

Then see the documents that Michael is referring to: https://patriots4truth.org/2019/05/24/unmasking-the-five-eyes-beast/