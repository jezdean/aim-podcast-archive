---
audio: https://express.sfo2.digitaloceanspaces.com/aim/Oc97K7vF9vs.m4a
audio_duration: 00:46:15
date: 2019-05-11 00:17:09
filename: Oc97K7vF9vs.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Spying 'n lying will be their demise
year: '2019'
---

Betsy and Thomas discuss three hot topics: Did Mueller hire Fusion GPS to write part of the Ca-Ca Dossier? Trump calls out John Kerry for Logan Act violations. Bill Barr is raising the bar his position as attorney general. 

The propaganda article by Paul Sperry can be found here: https://www.realclearinvestigations.com/articles/2019/05/08/who_were_the_mueller_reports_hired_guns.html