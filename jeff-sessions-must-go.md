---
audio: https://express.sfo2.digitaloceanspaces.com/aim/5-_N3lMIi4Q.m4a
audio_duration: 00:30:29
date: 2018-03-30 22:53:23
filename: 5-_N3lMIi4Q.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Jeff Sessions Must Go
year: '2018'
---

TRIGGER ALERT. We discuss a Q post #975 and explain why is was a message in the bottle to us. Watch us connect the dots between Jeff Sessions, John Huber, Michael Horowitz, Rod Rosenstein, and Senior Executive Services. 

This is an article we did on Sessions as we began to investigate his background: https://aim4truth.org/2017/12/29/corporate-transnational-warlord-pirates-are-on-the-run/

These are several audios that we have done on Sessions in the past: 

https://www.youtube.com/watch?v=eW1KAnAwIAw

https://www.youtube.com/watch?v=QLDE7XTxg24

https://www.youtube.com/watch?v=hTnyhCsbZ_w

Here is one on Michael Horowitz:

https://www.youtube.com/watch?v=4QkWnspDjIg