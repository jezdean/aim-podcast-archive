---
audio: https://express.sfo2.digitaloceanspaces.com/aim/fpfclYcua7w.m4a
audio_duration: 03:21:11
date: 2018-04-06 02:13:51
filename: fpfclYcua7w.m4a
playlist: AIM Insights
publisher: AIM
title: Douglas Gabriel Reveals All
year: '2018'
---

Douglas Gabriel is interviewed by Nathan from Lift the Veil. If you want to see the Skype version, please go to Nathan's site. https://www.youtube.com/watch?v=ZMg5H0UifDY&list=PL3CuMkGZhwsYSqdAY-3VVR_8Ff8yQXFVA