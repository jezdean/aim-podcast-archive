---
audio: https://express.sfo2.digitaloceanspaces.com/aim/Wd0_vPlzlxk.m4a
audio_duration: 00:30:07
date: 2019-02-26 19:07:39
filename: Wd0_vPlzlxk.m4a
playlist: AIM Insights
publisher: AIM
title: Are you spinning in a limited hangout?
year: '2019'
---

This is part one of two. In this audio you will learn techniques for spotting limited hangouts and controlled opposition. In the second video you will hear Douglas Gabriel and Michael McKibben analyze an actual hangout. It is a case study package that all informed patriots will not want to miss. 

Use this link to share both videos and support material with your audience: https://patriots4truth.org/2019/02/26/are-you-spinning-in-a-limited-hangout/