---
audio: https://express.sfo2.digitaloceanspaces.com/aim/qQ9smr6pSNM.m4a
audio_duration: 00:42:30
date: 2018-07-06 19:30:09
filename: qQ9smr6pSNM.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Trump Tweets Brave Heroes of ICE and Border Control
year: '2018'
---

Betsy and Thomas look at the Trump’s tweets of July 4-5, 2018. To read the tweets as you listen: https://truthbits.blog/2018/07/06/trump-tweets-brave-heroes-of-ice-and-border-patrol/