---
audio: https://express.sfo2.digitaloceanspaces.com/aim/szCTNH2HQcE.m4a
audio_duration: 00:24:05
date: 2017-10-05 18:53:24
filename: szCTNH2HQcE.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Equifax Fraud Intended to Put You in a Cyber Credit Box
year: '2017'
---

Thomas and Betsy explain how the Equifax fraud and theft of 143 million customer profiles is a sly way for Google's Alphabet to collect your personal information in order to keep you contained in a cyber credit box. Why is the IRS partnering with Equifax and what is the company FireEye's role in this breach? For those of you who are not familiar with the American Intelligence Media, please make sure to visit www.aim4truth.org and make sure you are receiving TRUTH NEWS HEADLINES every day.