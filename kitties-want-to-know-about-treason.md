---
audio: https://express.sfo2.digitaloceanspaces.com/aim/x_QtN2kJGcA.m4a
audio_duration: 00:55:45
date: 2018-09-07 21:21:49
filename: x_QtN2kJGcA.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Kitties want to know about treason
year: '2018'
---

As Betsy and Thomas decode the Trump Tweets of September 7, 2018, they get sidetracked on questions that Senator Lindsay Graham asked of COTUS nominee Brett Kavanaugh about military tribunals. To read the tweets and the other information mentioned in the audio: https://truthbits.blog/2018/09/07/why-is-everybody-talking-about-military-tribunals/