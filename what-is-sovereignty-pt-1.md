---
audio: https://express.sfo2.digitaloceanspaces.com/aim/what-is-sov.mp3
audio_duration: 01:23:28
date: 2020-09-10 00:00:00
filename: what-is-sov.mp3
publisher: AIM
title: What is Sovereignty? Pt. 1
year: '2020'
---

A Conversation with John Barnwell & Douglas Gabriel

https://www.youtube.com/watch?v=rfOVUxq-mlo