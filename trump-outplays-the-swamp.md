---
audio: https://express.sfo2.digitaloceanspaces.com/aim/IRhlvbCY1sU.m4a
audio_duration: 00:37:20
date: 2018-09-13 22:26:05
filename: IRhlvbCY1sU.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Trump Outplays the Swamp
year: '2018'
---

As Betsy and Thomas decode the Trump Tweets of September 13, 2018. Learn why Hillary Clinton has not been arrested. Learn how Trump’s newest executive order is a game changer. Read the tweets and the other information mentioned in the audio: https://truthbits.blog/2018/09/13/trump-outplays-the-swamp/