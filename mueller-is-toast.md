---
audio: https://express.sfo2.digitaloceanspaces.com/aim/2WUboRmfsiQ.m4a
audio_duration: 00:38:50
date: 2017-12-18 22:23:57
filename: 2WUboRmfsiQ.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Mueller is Toast
year: '2017'
---

Betsy and Thomas say this audio is the best one yet. There are no words to describe the places they go and the people they reveal. Just put the cursor anywhere and listen for 30 seconds.  The True Pundit article referenced is here: https://truepundit.com/comey-mueller-ignored-mccabes-ties-to-russian-crime-figures-his-reported-tampering-in-russian-fbi-cases-files/