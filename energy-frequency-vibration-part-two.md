---
audio: https://express.sfo2.digitaloceanspaces.com/aim/PZbKaaypCaM.m4a
audio_duration: 00:39:40
date: 2018-02-16 12:14:21
filename: PZbKaaypCaM.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Energy Frequency Vibration Part TWO
year: '2018'
---

This two-part conversation is for advanced Glass Bead Game students. Many subscribe to this channel so this was the best place to put this conversation for fellow truth seekers to savor. Part One can be found here: https://www.youtube.com/watch?v=9qWxqe3UgtA