---
audio: https://express.sfo2.digitaloceanspaces.com/aim/light-of-christ-in-our-heart.mp3
audio_duration: 00:53:46
date: 2020-04-04 00:00:00
filename: light-of-christ-in-our-heart.mp3
publisher: AIM
title: The Light of Christ
year: '2020'
---

Betsy and Thomas discuss some spiritual matters in this audio: The Light of Christ is in our Hearts.