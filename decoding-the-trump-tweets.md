---
audio: https://express.sfo2.digitaloceanspaces.com/aim/HNVAiyl-3c8.m4a
audio_duration: 00:20:50
date: 2018-05-25 23:56:18
filename: HNVAiyl-3c8.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Decoding the Trump Tweets
year: '2018'
---

Thomas recall their favorite skit with Johnny Carson on the Magnificent Carnac as they read through today’s tweets from the President.  

Follow the tweet read: https://truthbits.blog/2018/05/25/decoding-trump-tweets-may-25-2018/

To view the YouTube video that was taken down go here: https://truthbits.blog/2018/05/23/decoding-trump-tweets-may-22-23-2018/