---
audio: https://express.sfo2.digitaloceanspaces.com/aim/zoom-is-chinese-surveillance.mp3
audio_duration: 01:03:34
date: 2020-09-16 00:00:00
filename: zoom-is-chinese-surveillance.mp3
publisher: AIM
title: China is Watching Through Zoom
year: '2020'
---

Douglas, Michael, and Tyla explain why Zoom is a dangerous Chinese product designed to surveil Americans at work and play for the benefit of the Highlands Group, communist China, the Pilgrims Society, and the British Imperial Empire.



https://patriots4truth.org/2020/09/16/zoom-spies-on-the-world/