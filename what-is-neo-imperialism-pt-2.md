---
audio: https://express.sfo2.digitaloceanspaces.com/aim/what-is-neoimperialism-pt2.mp3
audio_duration: 01:19:56
date: 2020-06-22 00:00:00
filename: what-is-neoimperialism-pt2.mp3
publisher: AIM
title: What is Neo-Imperialism? Pt. 2
year: '2020'
---

What is Neo-Imperialism? Pt. 2 – John Barnwell & Douglas Gabriel explore the spiritual dimensions of the Patriots' struggle against the Elite Cabal in the modern world.
To help my ongoing research donate: https://www.paypal.me/johnbarnwell888
To purchase books by John Barnwell: https://jbarnwell.academia.edu
Further info: American Intelligence Media https://aim4truth.org