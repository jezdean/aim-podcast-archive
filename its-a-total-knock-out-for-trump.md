---
audio: https://express.sfo2.digitaloceanspaces.com/aim/fHUbkAG4pzQ.m4a
audio_duration: 00:43:00
date: 2018-08-24 00:15:26
filename: fHUbkAG4pzQ.m4a
playlist: Betsy and Thomas
publisher: AIM
title: It’s a Total Knock Out for Trump
year: '2018'
---

Listen to Betsy and Thomas explain how Trump is knocking out Sessions, Brennan, and the Senior Executive Service in a 1-2-3- punch.  Read the articles discussed here: https://patriots4truth.org/2018/08/24/its-a-total-knock-out-for-trump/