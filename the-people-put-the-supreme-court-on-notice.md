---
audio: https://express.sfo2.digitaloceanspaces.com/aim/M084afE7jtA.m4a
audio_duration: 00:50:30
date: 2018-10-31 23:34:08
filename: M084afE7jtA.m4a
playlist: Swamp with Strunk
publisher: AIM
title: The People put the Supreme Court on notice
year: '2018'
---

In this fascinating interview with AIM4Truth Christopher Strunk, https://patriots4truth.org/2018/10/31/the-people-put-the-supreme-court-on-notice/, we get the backstory on why Christopher and fellow petitioner Harold van Allen, decided to launch this information grenade into the swamp. Fitting thing to do for two Vietnam veterans - but now they see that the war we fight is with information, not explosives.

For years, these two vets have been honing their skills in lodging lawsuits at the Supreme Court. Just search their names on the internet.

Now, they join the AIM Conclave to take the information war to a higher level. Give them your support in the comment box below. 
Congratulations to these two AIM patriots for doing what we say all the time - do what you can, with the skills and talents you have, to educate and enlighten your circle of influence. Now, you go out and preach the same.

Truth is not linear. It is radiant. Got it?