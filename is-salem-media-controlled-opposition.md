---
audio: https://express.sfo2.digitaloceanspaces.com/aim/kei7_0D-DKM.m4a
audio_duration: 01:01:10
date: 2019-02-26 19:31:33
filename: kei7_0D-DKM.m4a
playlist: AIM Insights
publisher: AIM
title: Is Salem Media Controlled Opposition?
year: '2019'
---

Now that you learned about limited hangouts and controlled opposition in part one  https://youtu.be/Wd0_vPlzlxk , apply what you have learned in a case study of Salem Media. 

Pick up the media kit here which includes the two audios and the actual evidence that we pulled from the research mines: https://patriots4truth.org/2019/02/26/are-you-spinning-in-a-limited-hangout/