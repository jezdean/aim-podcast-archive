---
audio: https://express.sfo2.digitaloceanspaces.com/aim/P7Nwx38SxJI.m4a
audio_duration: 00:49:41
date: 2019-08-30 18:45:21
filename: P7Nwx38SxJI.m4a
playlist: AIM Insights
publisher: AIM
title: British Lies and British Spies
year: '2019'
---

AFI Michael McKibben and AIM Douglas Gabriel report on the roots of fake news which are found in a huge global British spy network that spews propaganda to promote never-ending war, big pharma eugenics, regime changes, and even…the overthrow attempt of Donald J. Trump. 

Learn more about Wellcome and the Wee Little Death Box here: https://truthbits.blog/2019/08/23/wellcome-and-the-wee-little-death-box/

See the AFI research to support these claims: [coming when report is finished. will be posted at www.aim4truth.org]