---
audio: https://express.sfo2.digitaloceanspaces.com/aim/mXHU8XdUSbY.m4a
audio_duration: 00:45:50
date: 2019-01-23 21:09:44
filename: mXHU8XdUSbY.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Insider Points to Vatican Operatives in Trump Overthrow Attempt
year: '2019'
---

Betsy and Thomas of the American Intelligence Media review an email they received from a source claiming to be a Vatican insider who explains how Robert Mueller, Joseph Mifsud, and Stephan Roh are all Knights of Malta agents working to overthrow Donald J. Trump.  Listen to the audio as you read the email attachment: https://patriots4truth.org/2019/01/23/insider-points-to-vatican-operatives-in-trump-overthrow-attempt/