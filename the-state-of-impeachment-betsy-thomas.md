---
audio: https://express.sfo2.digitaloceanspaces.com/aim/385339395.m4a
audio_duration: 00:58:52
date: 2020-01-16 15:48:18
filename: 385339395.mp3
publisher: AIM
title: The State of Impeachment - Betsy & Thomas
year: '2020'
---

Betsy and Thomas look at the state of impeachment in this week's episode of Drain the Swamp. For those interested in what Donald Trump has to say about John Roberts and to grab the link to all of our audios in one file, see:
patriots4truth.org/2020/01/15/what-does-president-trump-think-of-john-roberts/
sfo2.digitaloceanspaces.com/express/aim/index.html
See this week's CAT REPORT: aim4truth.org/2020/01/16/cat-report-273/