---
audio: https://express.sfo2.digitaloceanspaces.com/aim/AT3rH056CVg.m4a
audio_duration: 00:35:04
date: 2018-07-26 19:14:28
filename: AT3rH056CVg.m4a
playlist: AIM Insights
publisher: AIM
title: Ban all electronic voting machines NOW
year: '2018'
---

Douglas Gabriel and Michael McKibben explain why Devin Nunes’ call to ban electronic voting systems is critical for free, fair, and transparent elections. Then continue your education as an informed citizen by reading this collection of articles: https://truthbits.blog/2018/07/26/ban-all-electronic-voting-machines-now/