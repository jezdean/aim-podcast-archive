---
audio: https://express.sfo2.digitaloceanspaces.com/aim/9qWxqe3UgtA.m4a
audio_duration: 00:49:24
date: 2018-02-16 03:21:18
filename: 9qWxqe3UgtA.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Energy Frequency and Vibration Part ONE
year: '2018'
---

This two-part conversation is for advanced Glass Bead Game students. Many subscribe to this channel so this was the best place to put this conversation for fellow truth seekers to savor. PART TWO: https://www.youtube.com/watch?v=PZbKaaypCaM