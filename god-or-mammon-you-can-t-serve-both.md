---
audio: https://express.sfo2.digitaloceanspaces.com/aim/WwSz1Ltw5Wo.m4a
audio_duration: 00:37:31
date: 2019-04-03 19:57:18
filename: WwSz1Ltw5Wo.m4a
playlist: Betsy and Thomas
publisher: AIM
title: God or mammon? You can't serve both
year: '2019'
---

Access the full lesson on banking with all audios here: https://aim4truth.org/2019/04/03/god-or-mammon/