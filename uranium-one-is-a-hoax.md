---
audio: https://express.sfo2.digitaloceanspaces.com/aim/n6aCYJySoGE.m4a
audio_duration: 00:33:23
date: 2017-11-02 21:11:36
filename: n6aCYJySoGE.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Uranium One is a Hoax
year: '2017'
---

Nobody is going to put it all together like our 'Thomas Paine'. But once our readers and listeners understand the big picture, there is no calling back the image of truth. Learn the deep connections between Bill Clinton, George H.W. Bush, Tenam, Frank Guistra, the Russians, and what is really going on at the United States Enrichment Corporation in Paducah, Kentucky.