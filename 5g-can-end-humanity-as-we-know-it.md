---
audio: https://express.sfo2.digitaloceanspaces.com/aim/1XcpPcMdhxM.m4a
audio_duration: 00:21:00
date: 2018-10-18 13:03:34
filename: 1XcpPcMdhxM.m4a
playlist: AIM Insights
publisher: AIM
title: 5G can end humanity as we know it
year: '2018'
---

5G is no laughing matter. We need to understand what this technology will do to the human race, from physical to spiritual harms. After you listen to Michael McKibben and Douglas Gabriel discuss this subject, learn more about the technology and ways that you can protect you and your family: https://patriots4truth.org/2018/10/18/5g-can-end-humanity-as-we-know-it/