---
audio: https://express.sfo2.digitaloceanspaces.com/aim/FxRLVj8z2x4.m4a
audio_duration: 00:57:47
date: 2018-12-01 22:29:52
filename: FxRLVj8z2x4.m4a
playlist: Betsy and Thomas
publisher: AIM
title: AIM cats say good-bye to George H. W. Bush
year: '2018'
---

George H. W. Bush died. Thomas gives this AIM eulogy.  Listen to the audio, read the tweets, learn more about feline propaganda: https://truthbits.blog/2018/12/01/eulogy-to-george-h-w-bush-how-do-you-say-goodbye-to-a-mass-murderer/