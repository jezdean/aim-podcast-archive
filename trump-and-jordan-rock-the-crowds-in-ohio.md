---
audio: https://express.sfo2.digitaloceanspaces.com/aim/V2P6f6iTx1o.m4a
audio_duration: 00:47:28
date: 2018-08-05 19:26:59
filename: V2P6f6iTx1o.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Trump and Jordan Rock the Crowds in Ohio
year: '2018'
---

Betsy and Thomas decode Trump’s tweets of August 5, 2018 and discuss the Ohio rally where Trump’s crowd went wild for Jim Jordan.  To read the tweets as you listen and see other interesting information:  https://truthbits.blog/2018/08/05/trump-and-jordan-rock-the-crowds-in-ohio/