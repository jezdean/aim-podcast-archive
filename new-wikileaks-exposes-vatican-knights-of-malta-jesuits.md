---
audio: https://express.sfo2.digitaloceanspaces.com/aim/mml1QcRZjxE.m4a
audio_duration: 00:35:06
date: 2019-01-31 00:42:13
filename: mml1QcRZjxE.m4a
playlist: Betsy and Thomas
publisher: AIM
title: New Wikileaks Exposes Vatican, Knights of Malta, Jesuits
year: '2019'
---

Documents released by WikiLeaks today shed light on a power struggle within the highest offices of the Catholic Church.

AIM Patriot John S. asked Douglas to provide an explanation of what this material means. Here is a link that contains the Wikileaks and support materials on Pope Francis: https://aim4truth.org/2019/01/30/new-wikileaks-exposes-vatican-knights-of-malta-jesuits-and-pope-frances/