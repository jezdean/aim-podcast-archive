---
audio: https://express.sfo2.digitaloceanspaces.com/aim/5eWlx0wx00g.m4a
audio_duration: 01:09:59
date: 2019-01-20 20:11:21
filename: 5eWlx0wx00g.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Trump Tweets reveal a HUGE secret
year: '2019'
---

Yes, this audio is over an hour…that’s because Thomas had so much to say about Mueller’s predicament that we just kept the audio going. In the beginning he and Betsy talk about Trump tweets of January 20, 2019…but one thing leads to another and then it all leads to Felix Sater. Read the tweets here and learn more: https://truthbits.blog/2019/01/20/trump-tweets-reveal-a-huge-secret/