---
audio: https://express.sfo2.digitaloceanspaces.com/aim/mJRpeq0s9Ls.m4a
audio_duration: 00:35:01
date: 2018-03-24 18:38:24
filename: mJRpeq0s9Ls.m4a
playlist: Betsy and Thomas
publisher: AIM
title: SES rats on the run
year: '2018'
---

Thomas has a response to George Webb who challenged Douglas on the legitimacy of the Senior Executive Services and the DOJ 500. Gloves off. Let’s rumble. George, ball in your court. 

Here is George’s video: https://www.youtube.com/watch?v=yG6SFmDkpaQ