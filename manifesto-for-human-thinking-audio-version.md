---
audio: https://express.sfo2.digitaloceanspaces.com/aim/iqIsOfXxNeA.m4a
audio_duration: 00:49:24
date: 2019-02-09 02:10:36
filename: iqIsOfXxNeA.m4a
playlist: AIM Insights
publisher: AIM
title: Manifesto for human thinking- audio version
year: '2019'
---

Read the manifesto here: https://neoanthroposophy.com/2019/02/08/manifesto-to-save-human-thinking/