---
audio: https://express.sfo2.digitaloceanspaces.com/aim/P6KzVC1ArGI.m4a
audio_duration: 00:40:08
date: 2018-09-28 01:04:57
filename: P6KzVC1ArGI.m4a
playlist: Betsy and Thomas
publisher: AIM
title: The Senate Judiciary Circus is in Town
year: '2018'
---

Betsy and Thomas review today’s Senate Judiciary Committee’s hearings of Christine Blasey Ford and Brett Kavanaugh. An article of interest is here: https://aim4truth.org/2018/09/27/heres-why-judiciary-want-an-fbi-investigation-of-kavanaugh-guess-who-holds-the-encryption-keys/