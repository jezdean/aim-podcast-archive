---
audio: https://express.sfo2.digitaloceanspaces.com/aim/P9nS2FFplgU.m4a
audio_duration: 00:50:33
date: 2019-06-15 20:19:12
filename: P9nS2FFplgU.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Comey-Clinton Cover-up Exposed
year: '2019'
---

Betsy and Thomas do a deep dive into some very interesting tweets from Trump in mid-June 2019. 

To read the tweets we selected, click here: (https://truthbits.blog/2019/06/15/comey-clinton-cover-up-exposed/