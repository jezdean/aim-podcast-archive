---
audio: https://express.sfo2.digitaloceanspaces.com/aim/ppaxsBkLNHQ.m4a
audio_duration: 00:36:24
date: 2017-12-22 02:09:27
filename: ppaxsBkLNHQ.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Nothing Can Stop the Trump Train
year: '2017'
---

Betsy and Thomas call out the actors who want to reauthorize FISA Section 702 Reauthorization – Daniel Coats, Jeff Sessions, Mike Pompeo, Christopher Wray, and Michael Rogers. Americans are sick and tire of 17 intelligence agencies spying on them using this unconstitutional work-around to surveil all of us all the time. Enough is enough.  

Articles for reference: 

https://aim4truth.org/2017/12/19/big-brother-is-watching-you-for-real/

https://aim4truth.org/2017/11/24/absolute-proof-obama-not-russians-rigged-elections/