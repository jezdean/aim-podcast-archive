---
audio: https://express.sfo2.digitaloceanspaces.com/aim/wS1IxtAI7hM.m4a
audio_duration: 00:53:57
date: 2018-03-24 21:04:44
filename: wS1IxtAI7hM.m4a
playlist: AIM Insights
publisher: AIM
title: Trump's new cabinet
year: '2018'
---

Who is John Bolton and will he clean the NSC leaks coming from the White House office? Thomas Paine and John Barnwell discuss Trump’s new cabinet appointments and the exit of the globalists.