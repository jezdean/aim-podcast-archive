---
audio: https://express.sfo2.digitaloceanspaces.com/aim/CIXxqynQoIY.m4a
audio_duration: 00:34:55
date: 2018-10-19 12:40:24
filename: CIXxqynQoIY.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Trump could end it all in a BLINK
year: '2018'
---

Over the last few days, Douglas and Michael had the research team deep dive into China – from their surveillance state system to its mega company Alibaba. After you listen to this video, make sure to continue your citizen education here:  https://truthbits.blog/2018/10/19/china-is-our-enemy-learn-why/