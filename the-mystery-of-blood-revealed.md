---
audio: https://express.sfo2.digitaloceanspaces.com/aim/n7Gg7RXcJXI.m4a
audio_duration: 00:41:28
date: 2019-02-26 02:33:23
filename: n7Gg7RXcJXI.m4a
playlist: AIM Insights
publisher: AIM
title: The Mystery of Blood Revealed
year: '2019'
---

Douglas, Michael, and Tyla are settling in for a few days of audios for our amazing truth community that is winning the information war on all fronts. The night before we start recording, we like to sit around and warm up the conversation. This time, we thought you might like to join us. We know it can be lonely out there to find thinkers like you and us....and this conversation took some interesting twists and turns.