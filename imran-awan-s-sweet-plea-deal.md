---
audio: https://express.sfo2.digitaloceanspaces.com/aim/dPK45t1V8oM.m4a
audio_duration: 00:19:52
date: 2018-07-03 21:18:17
filename: dPK45t1V8oM.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Imran Awan's Sweet Plea Deal
year: '2018'
---

Is Judge Tonya Chutkin going to accept the Imran Awan plea deal of one count of bank fraud? Prosecutor thinks that is a fair deal. Betsy and Thomas are outraged. 

See here for the article on Awan: http://www.foxnews.com/politics/2018/07/03/ex-dem-it-aide-imran-awan-pleads-guilty-to-bank-fraud-in-deal-with-prosecutors.html