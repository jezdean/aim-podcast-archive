---
audio: https://express.sfo2.digitaloceanspaces.com/aim/389067608.m4a
audio_duration: 01:00:07
date: 2020-02-03 13:31:54
filename: 389067608.mp3
publisher: AIM
title: What you need to know about the flu season and pandemics
year: '2020'
---