---
audio: https://express.sfo2.digitaloceanspaces.com/aim/the-storm-is-on-us.mp3
audio_duration: 00:51:56
date: 2020-03-19 14:14:00
filename: the-storm-is-on-us.mp3
publisher: AIM
title: 'Black Swan: The Global Reset is Coming'
year: '2020'
---

Betsy and Thomas bring you to date on coronavirus and the collapse of the supply chain and banking system.