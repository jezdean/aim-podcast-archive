---
audio: https://express.sfo2.digitaloceanspaces.com/aim/uZppejfixMI.m4a
audio_duration: 00:17:25
date: 2018-10-05 17:24:06
filename: uZppejfixMI.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Jim Jordan Pulls the Plug on the Swamp
year: '2018'
---

Betsy and Thomas are as excited as Representative Jim Jordan about what swamp attorney James Baker revealed in his closed-door hearing. Make sure to watch the video of Jim here: https://truthbits.blog/2018/10/05/jim-jordan-pulls-the-plug-on-the-swamp/