---
audio: https://express.sfo2.digitaloceanspaces.com/aim/LLjubZ0lXDs.m4a
audio_duration: 00:37:09
date: 2018-06-20 22:34:49
filename: LLjubZ0lXDs.m4a
playlist: Betsy and Thomas
publisher: AIM
title: More Trump Tweets Decoded
year: '2018'
---

Betsy and Thomas look at the Trump’s tweets of June 20. To read the tweets as you listen: https://truthbits.blog/2018/06/20/decoding-trump-tweets-june-20-2018/