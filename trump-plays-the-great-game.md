---
audio: https://express.sfo2.digitaloceanspaces.com/aim/QmXDJK_QaOg.m4a
audio_duration: 01:02:05
date: 2018-04-28 18:25:58
filename: QmXDJK_QaOg.m4a
playlist: AIM Insights
publisher: AIM
title: Trump Plays the Great Game
year: '2018'
---

Douglas Gabriel and John Barnwell discuss these topics. The British roots of the mechanisms of the U. S. secret government are put into historical perspective with the current U. S. shadow government working through the Senior Executive Service, Serco, Crown Agents and their connections to the CityofLondonUK. The president is "Trumping the Deep State" through using the strategies of The Art of the Deal mixed with his own brand of counterintelligence.