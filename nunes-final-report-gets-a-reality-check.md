---
audio: https://express.sfo2.digitaloceanspaces.com/aim/kquKPowFC7c.m4a
audio_duration: 00:26:55
date: 2018-04-29 10:29:34
filename: kquKPowFC7c.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Nunes Final Report Gets a Reality Check
year: '2018'
---

The full article is here: (coming in just a moment)

Betsy and Thomas take a good hard look at the Nunes Final Report that concludes that there was no collusion between the Trump campaign and the Russians. 

Diane Feinstein’s Strange Bedfellows: https://truthbits.blog/2018/04/27/diane-feinsteins-strange-bedfellows/