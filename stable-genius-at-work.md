---
audio: https://express.sfo2.digitaloceanspaces.com/aim/k7Y5Ihfm_wc.m4a
audio_duration: 00:24:16
date: 2018-04-16 18:42:24
filename: k7Y5Ihfm_wc.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Stable Genius at Work
year: '2018'
---

Donald Trump is the ultimate Pied Piper as the D.C. swamp rats get in line and follow his tune…right out of the swamp.