---
audio: https://express.sfo2.digitaloceanspaces.com/aim/ChPl7Ow8U2A.m4a
audio_duration: 00:47:00
date: 2017-08-17 20:23:40
filename: ChPl7Ow8U2A.m4a
playlist: Betsy and Thomas
publisher: AIM
title: 2017 Eclipse Reveals Trump as Phoenix Rising from Fire
year: '2017'
---

Thomas and Betsy take listeners on a new geopolitical adventure – this one involves astrology and astrosophy. Thomas, who guided Nancy Reagan as one of the White House’s astrologer-clairvoyants as President Reagan moved through stages of Alzheimer’s, brushes off his talents and applies them to Donald Trump’s chart.  
Thomas offers an analysis of the effects of the 2017 eclipse on individuals, America, and Trump. The President’s personal astrological chart is compared to the eclipse chart to predict what the effects and likely outcomes might be for the coming year for Trump and America.  "Cosmic" aspects of eclipses are explained and the cycle of annual eclipses indicates that we are in a fiery cycle of change until January 2018. Trump loves the fire and is rising like a Phoenix from the ashes of the Deep State.
The two articles that Thomas mentioned in his talk are: 

http://themillenniumreport.com/2017/08/the-great-american-eclipse-a-defining-moment-in-u-s-history/

http://cosmicconvergence.org/?p=21369

Make sure you are receiving your daily TRUTH NEWS HEADLINES in your inbox. Subscribe at www.aim4truth.org.