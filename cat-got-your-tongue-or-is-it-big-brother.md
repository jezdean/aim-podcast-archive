---
audio: https://express.sfo2.digitaloceanspaces.com/aim/LEpyRC8pHOQ.m4a
audio_duration: 00:33:20
date: 2018-06-11 00:29:00
filename: LEpyRC8pHOQ.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Cat got your tongue..or is it Big Brother?
year: '2018'
---

Thomas gave advanced students of the American Intelligence Media a homework assignment and this audio is his explanation. To see the assignment, click here: https://truthbits.blog/2018/06/11/cat-got-your-tongue-or-is-it-big-brother/