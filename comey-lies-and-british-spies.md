---
audio: https://express.sfo2.digitaloceanspaces.com/aim/AsHu4xl37dk.m4a
audio_duration: 00:53:20
date: 2019-09-06 21:29:36
filename: AsHu4xl37dk.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Comey Lies and British Spies
year: '2019'
---

Read about the 28 treasonous crimes of James Comey: https://aim4truth.org/2019/02/17/citizens-charge-james-comey-with-28-counts-of-treason/

Learn more history and current events that matter: https://truthbits.blog/2019/09/06/comey-lies-and-british-spies/