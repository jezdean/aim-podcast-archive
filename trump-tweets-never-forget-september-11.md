---
audio: https://express.sfo2.digitaloceanspaces.com/aim/vE9t21esACQ.m4a
audio_duration: 00:47:58
date: 2018-09-11 19:17:17
filename: vE9t21esACQ.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Trump Tweets - Never forget September 11
year: '2018'
---

As Betsy and Thomas decode the Trump Tweets of September 11, 2018 which requires some deep decoding to get to the truth of what happened this day in 2001. Read the tweets and the other information mentioned in the audio: https://truthbits.blog/2018/09/11/no-collusion-absolutely-zero/