---
audio: https://express.sfo2.digitaloceanspaces.com/aim/QdHEWv4Hd4M.m4a
audio_duration: 00:27:21
date: 2018-10-12 18:51:55
filename: QdHEWv4Hd4M.m4a
playlist: AIM Insights
publisher: AIM
title: Weather Terrorism Obliterates MAGA Panhandle
year: '2018'
---

Douglas Gabriel introduces MICHAEL THOMAS, the founder and editor of two of the independent media's most substantive news sites - State of the Nation and The Millennium Report.

As a resident of Florida, a first-hand eye witness of the devastation of Hurricane Michael, and a citizen journalist who has followed the progress of weather being weaponized against citizens, Michael is outraged at what is happening in Florida's panhandle just as the midterms are around the corner.

After listening to the audio, see this link to continue your education: https://patriots4truth.org/2018/10/12/globalists-use-weather-warfare-to-take-out-florida-maga-voters/