---
audio: https://express.sfo2.digitaloceanspaces.com/aim/2bFMT21mY3E.m4a
audio_duration: 00:40:40
date: 2019-06-14 14:51:46
filename: 2bFMT21mY3E.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Facebook Insider CONFESSES ALL
year: '2019'
---

Betsy reads a letter from a Facebook insider who claims to know the real truth about why and who created Facebook. To read the letter and use the translation feature on the page to convert to a language of your choice: https://aim4truth.org/2019/06/13/facebook-insider-confesses-all/