---
audio: https://express.sfo2.digitaloceanspaces.com/aim/dolkuwmfiO4.m4a
audio_duration: 00:41:47
date: 2018-09-30 19:58:20
filename: dolkuwmfiO4.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Trump tweets some bad boys
year: '2018'
---

Betsy and Thomas review Trump Tweets of September 28 and s9, 2018, but first go back to a few tweets on September 26 that need further decoding. To read the tweets and see an example of a video meme: 
https://truthbits.blog/2018/09/30/trump-tweets-some-bad-boys/