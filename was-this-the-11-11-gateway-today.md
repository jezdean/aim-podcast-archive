---
audio: https://express.sfo2.digitaloceanspaces.com/aim/UyodwIRR2Jo.m4a
audio_duration: 00:30:41
date: 2019-06-30 02:26:57
filename: UyodwIRR2Jo.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Was this THE 11:11 Gateway today?
year: '2019'
---

Listen to BPEarthWatch explain a solar event that occurred today. Then listen to Betsy and Thomas explain why this even was so critical today and how prayers around the world may have saved the sun from blinking out altogether. It sounds crazy...but the charts don't lie. It happened. The timing was exact. Was this the 11:11 gateway?

See the BPEarthWatch video here: https://youtu.be/jWHT8wq9TqM