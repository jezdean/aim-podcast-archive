---
audio: https://express.sfo2.digitaloceanspaces.com/aim/ztNvAqC2oLI.m4a
audio_duration: 00:29:48
date: 2018-06-08 18:25:45
filename: ztNvAqC2oLI.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Trump Tweets Target Macron and Trudeau
year: '2018'
---

Betsy and Thomas review Trump Tweets of June 8. Link here for audio and tweets in one URL: https://truthbits.blog/2018/06/08/trudeau-and-macron-big-daddy-headed-your-way/

Bypass YouTube altogether and follow www.truthbits.blog to receive Trump Tweet decodes as they are posted. Audio and tweets all on one page. Plus, special surprises at the bottom of the page.