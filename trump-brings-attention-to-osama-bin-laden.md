---
audio: https://express.sfo2.digitaloceanspaces.com/aim/42CTNVvhPSU.m4a
audio_duration: 00:40:36
date: 2018-11-20 19:28:09
filename: 42CTNVvhPSU.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Trump brings attention to Osama Bin Laden
year: '2018'
---

Betsy and Thomas decode Trump tweets of November 19, 2018…plus give listeners some food for thought about Donald Rumsfeld, George Bush, Dick Cheney, Bob Mueller, and John Brennan. Read the tweets and continue your citizen education: https://truthbits.blog/2018/11/20/the-christmas-tree-arrives-and-pakistan-is-busted/