---
audio: https://express.sfo2.digitaloceanspaces.com/aim/1U7NSu832Q4.m4a
audio_duration: 00:46:52
date: 2018-07-09 21:56:30
filename: 1U7NSu832Q4.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Troller-In-Chief Dominates Twitter
year: '2018'
---

Betsy and Thomas explain why Trump’s tweets of July 8 and 9, 2018 show that he is a master twitter troll. To read the tweets as you listen: https://truthbits.blog/2018/07/09/troller-in-chief-dominates-twitter/