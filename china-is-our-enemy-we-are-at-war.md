---
audio: https://express.sfo2.digitaloceanspaces.com/aim/eM7pQsVtqXE.m4a
audio_duration: 00:32:01
date: 2018-10-05 18:54:47
filename: eM7pQsVtqXE.m4a
playlist: Betsy and Thomas
publisher: AIM
title: China is our enemy. We are at war.
year: '2018'
---

Betsy and Thomas explain why VP Mike Pence announced that China is an enemy  of the United States. Make sure to watch the video of Mike Pence or read the speech transcript here: https://truthbits.blog/2018/10/05/mike-pence-talks-tough-on-china/