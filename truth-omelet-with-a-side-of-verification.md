---
audio: https://express.sfo2.digitaloceanspaces.com/aim/wAn8F_78RpQ.m4a
audio_duration: 01:02:55
date: 2018-09-04 20:19:58
filename: wAn8F_78RpQ.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Truth omelet with a side of verification
year: '2018'
---

Betsy and Thomas decode the Trump Tweets of September 3 and 4, 2018. Trump calls out the failing New York Times, and NBC fake news. He also gives a shot across the bow to Bashar al-Assad. To read the tweets and the other information mentioned in the audio: https://truthbits.blog/2018/09/04/delicious-truth-omelet-with-a-side-of-verification/