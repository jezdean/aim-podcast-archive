---
audio: https://express.sfo2.digitaloceanspaces.com/aim/8b8XQCLVpeE.m4a
audio_duration: 00:39:54
date: 2018-09-25 22:28:52
filename: 8b8XQCLVpeE.m4a
playlist: Betsy and Thomas
publisher: AIM
title: The United Nations' Party is Over
year: '2018'
---

Thomas and Betsy look at POTUS tweets of September 24 and 25, 2018 and review his BRILLIANT speech given at the United Nations. TO read the tweets, see the speech, and other surprises: https://truthbits.blog/2018/09/25/the-united-nations-party-is-over/