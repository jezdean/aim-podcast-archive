---
audio: https://express.sfo2.digitaloceanspaces.com/aim/qpXNLMPtOnE.m4a
audio_duration: 00:34:49
date: 2018-05-31 17:12:27
filename: qpXNLMPtOnE.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Spies, Informants, and Double Standards
year: '2018'
---

Betsy and Thomas review Trump Tweets of May 30-31, 2018. Trump continue to tweets about Jeff Session’s incompetency as AG, using words from other spokespeople. Follow along at this link which is also very easy to pass along in your social media network. https://truthbits.blog/2018/05/31/decoding-trump-tweets-may-31-2018/

Bypass YouTube altogether and follow www.truthbits.blog to receive Trump Tweet decodes as they are posted. Audio and tweets all on one page. Plus, special surprises at the bottom of the page.