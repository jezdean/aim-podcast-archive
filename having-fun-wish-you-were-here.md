---
audio: https://express.sfo2.digitaloceanspaces.com/aim/yHHLRivFmBQ.m4a
audio_duration: 00:43:20
date: 2018-07-13 20:38:44
filename: yHHLRivFmBQ.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Having fun. Wish you were here.
year: '2018'
---