---
audio: https://express.sfo2.digitaloceanspaces.com/aim/NFOlxbrLGGA.m4a
audio_duration: 00:30:41
date: 2018-06-09 19:50:28
filename: NFOlxbrLGGA.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Europe Union to silence its Subjects
year: '2018'
---

Betsy and Thomas discuss the increasing loss of free speech in Europe and America. 
The articles that are referenced are: https://aim4truth.org/2017/03/05/overthrowing-america-in-twelve-steps/
https://patriots4truth.org/2017/12/28/how-memes-and-hastags-are-changing-the-nature-of-warfare/