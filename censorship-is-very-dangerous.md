---
audio: https://express.sfo2.digitaloceanspaces.com/aim/Tvhy2j5hlBc.m4a
audio_duration: 00:44:18
date: 2018-08-18 23:50:06
filename: Tvhy2j5hlBc.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Censorship is very dangerous
year: '2018'
---

Betsy and Thomas decode Trump’s tweets of August 17 and 18, 2018. POTUS slams social media, censorship, Bruce and Nelly Ohr, BLANK Jeff Sessions, and John Brennan in this tweetblast. To read the tweets as you listen and see other interesting information: https://truthbits.blog/2018/08/18/potus-tweets-censorship-is-dangerous/