---
audio: https://express.sfo2.digitaloceanspaces.com/aim/sXAIP5eCdFw.m4a
audio_duration: 00:39:01
date: 2018-10-04 19:49:09
filename: sXAIP5eCdFw.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Kavanaugh Vote Rallies the Base
year: '2018'
---

Betsy and Thomas review Trump Tweets regarding Brett Kavanaugh by answering reader questions. They also discuss how Germany and the EU need to wake up to George Soros.  To read the tweets and see a great post on Blasey-Ford: https://truthbits.blog/2018/10/04/kavanaugh-vote-rallies-the-base/