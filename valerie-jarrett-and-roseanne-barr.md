---
audio: https://express.sfo2.digitaloceanspaces.com/aim/ddMqwfurKOQ.m4a
audio_duration: 00:42:05
date: 2018-05-30 19:20:57
filename: ddMqwfurKOQ.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Valerie Jarrett and Roseanne Barr
year: '2018'
---

Betsy and Thomas review Trump Tweets of May 30, 2018. Trump tweets about Trey Gowdy, Jeff Sessions, Valerie Jarrett, and Roseanne Barr. Today’s tweets and article references can be read here: https://truthbits.blog/2018/05/30/decoding-trump-tweets-may-30-2018/

Bypass YouTube altogether and follow www.truthbits.blog to receive Trump Tweet decodes as they are posted. Audio and tweets all on one page. Plus, special surprises at the bottom of the page.