---
audio: https://express.sfo2.digitaloceanspaces.com/aim/fe4iOlIoffY.m4a
audio_duration: 00:46:21
date: 2018-10-16 19:39:36
filename: fe4iOlIoffY.m4a
playlist: Betsy and Thomas
publisher: AIM
title: 'Trump asks: "Where is Jeff Sessions?"'
year: '2018'
---

Betsy and Thomas review Trump Tweets of October 16, 2018 where POTUS calls out Horseface and Bruce and Nellie Ohr…plus WHERE IS JEFF SESSIONS? Continue your citizen education here: https://truthbits.blog/2018/10/16/trump-asks-where-is-jeff-sessions/