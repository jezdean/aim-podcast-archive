---
audio: https://express.sfo2.digitaloceanspaces.com/aim/kDrajHT9SDY.m4a
audio_duration: 00:08:34
date: 2018-06-07 19:40:55
filename: kDrajHT9SDY.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Trump's DIET Plan for the Swamp
year: '2018'
---

Make sure to check out this article on USAID: https://aim4truth.org/2018/04/24/hop-on-board-the-opic-usaid-corporate-gravy-train-operated-by-senior-executive-service-fuel-by-u-s-taxpayers/

Trump reorganize the swamps: http://amp.dailycaller.com/2018/06/06/trump-may-be-totally-reorganizing-the-federal-bureaucracy/?__twitter_impression=true