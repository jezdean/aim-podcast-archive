---
audio: https://express.sfo2.digitaloceanspaces.com/aim/iyZG1sONUSQ.m4a
audio_duration: 00:47:58
date: 2018-07-03 20:27:32
filename: iyZG1sONUSQ.m4a
playlist: Betsy and Thomas
publisher: AIM
title: New Tweets Have Arrived
year: '2018'
---

Betsy and Thomas look at the Trump’s tweets of July 1 and 2, 2018. To read the tweets as you listen: https://truthbits.blog/2018/07/03/your-trump-tweet-package-has-arrived/