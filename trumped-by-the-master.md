---
audio: https://express.sfo2.digitaloceanspaces.com/aim/8N-hxhWWRfY.m4a
audio_duration: 00:16:08
date: 2018-06-21 02:06:52
filename: 8N-hxhWWRfY.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Trumped by the Master
year: '2018'
---

Betsy and Thomas give you some very exciting news about the order that Trump just signed. Here is the EO link: https://www.whitehouse.gov/presidential-actions/affording-congress-opportunity-address-family-separation/?utm_source=twitter&utm_medium=social&utm_campaign=wh