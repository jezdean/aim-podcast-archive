---
audio: https://express.sfo2.digitaloceanspaces.com/aim/fM2on-gDu7M.m4a
audio_duration: 00:50:48
date: 2018-07-13 12:26:49
filename: fM2on-gDu7M.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Cheesecake and Covfefe with the Conclave
year: '2018'
---

Lots of working notes and theories on the Conclave drawing board right now. Betsy sneaks a microphone into their discussions so that you can hear how intellectuals like Douglas Gabriel and Michael McKibben develop their working theories and hypotheses that later become blockbuster citizens intelligence reports.