---
audio: https://express.sfo2.digitaloceanspaces.com/aim/initiation-and-ego-mastery.mp3
audio_duration: 00:36:51
date: 2020-07-10 00:00:00
filename: initiation-and-ego-mastery.mp3
publisher: AIM
title: Initiation and Ego Mastery
year: '2020'
---

Enjoy this discussion between the Gabriels and read here for some lecture notes on this topic: Initiation and Ego Mastery.

https://neoanthroposophy.com/2020/07/10/initiation-and-ego-mastery/