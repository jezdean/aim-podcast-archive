---
audio: https://express.sfo2.digitaloceanspaces.com/aim/XxtcnE6X2to.m4a
audio_duration: 00:45:28
date: 2018-03-16 01:01:48
filename: XxtcnE6X2to.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Turn off Their Revenue Stream
year: '2018'
---

Betsy was thrilled when Jonathan, the youngest Conclave member, dropped by today and gave her a huge tip on how to block all ads -- Google, Facebook, YouTube, Chrome, you name it. Not just an "ad block" system you may be using, but one that totally removed all ads - like a power scrubber.

Since Silicon Valley Tech Lords have taken away ad revenue for many of our great alt media sites, let's hit them back and turn off every single ad that comes to us. This product is totally free. 

Betsy thinks we have a patriotic duty to tell everybody we know about this great product. You don't have to go into all of the politics. Just show them how easy it is to install and how clean and nice their computer screen looks.

Jonathan describes how to use uBlock Origin to Betsy and Thomas in the video below.

To click on the product for instant installation, go here:
 
https://chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm?hl=en

https://patriots4truth.org/2018/03/16/lets-turn-off-their-revenue-stream/