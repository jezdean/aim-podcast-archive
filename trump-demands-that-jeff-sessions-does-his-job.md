---
audio: https://express.sfo2.digitaloceanspaces.com/aim/Tb2PB2tqDmQ.m4a
audio_duration: 00:37:25
date: 2018-08-24 23:13:19
filename: Tb2PB2tqDmQ.m4a
playlist: Betsy and Thomas
publisher: AIM
title: Trump Demands that Jeff Sessions Does His Job
year: '2018'
---

Betsy and Thomas decode Trump’s tweets of August 24, 2018. Trump calls out Jeff Sessions to do his job and demands that he open up the papers and documents without redaction. To read the tweets as you listen and see other interesting information: https://truthbits.blog/2018/08/24/trump-demands-that-jeff-sessions-does-his-job/